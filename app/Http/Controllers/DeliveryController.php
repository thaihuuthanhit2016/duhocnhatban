<?php
namespace App\Http\Controllers;
date_default_timezone_set("Asia/Bangkok");



use App\Models\Bill;
use App\Models\Bill_detail;
use App\Models\Brands_product;
use App\Models\Brands_product_shop;
use App\Models\Category_product;
use App\Models\Category_product_shop;
use App\Models\City;
use App\Models\Customer;
use App\Models\Feeship;
use App\Models\Payment;
use App\Models\Product;
use App\Models\Product_images;
use App\Models\Product_shop;
use App\Models\Province;
use App\Models\Shop;
use App\Models\Slider;
use App\Models\Wards;
use Cart;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB;
use Session;
use PDF;
class DeliveryController extends Controller
{




    public function getAddCategory(Request $request){
        $fee_ship=new Feeship();
        $fee_ship->fee_matp=$request->city;
        $fee_ship->fee_maqh=$request->province;
        $fee_ship->fee_xaid=$request->wards;
        $fee_ship->fee_fee_ship=$request->fee_ship;
        $fee_ship->save();





    }

    public function getEditCategory(){

        $city=City::orderby('matp','ASC')->get();
        return view('admin.delivery.addCategory',compact('city'));


    }

    public function getProvince(Request $request){
        $city=$request->ma_tp;
        $action=$request->action;
        $maid=$request->ma_id;


        $province=Province::where('matp',$city)->orderby('maqh','asc')->get();
        $province_html='';
        if ($action=='city'){
            $province_html.='                                <option value="">Chọn Quận Huyện</option>';

            foreach ($province as $p) {
                $province_html .= "<option value='";$province_html.= $p->maqh ;$province_html.="'>";$province_html.=$p->name_quanhuyen;$province_html.="</option>";
            }
            return $province_html;
        }else{

            $province=Wards::where('maqh',$maid)->orderby('xaid','asc')->get();
            $province_html.='                                <option value="">Chọn Xã Phường</option>';

            foreach ($province as $p) {
                $province_html .= "<option value='" . $p->xaid . "'>.$p->name_xaphuong.</option>";
            }
            return $province_html;

        }

    }

    public function getWards(Request $request){
        $provice=$request->province;

        $province=Province::where('matp',$provice)->orderby('xaid','asc')->get();
        $province_html='';
        foreach ($province as $p) {
            $province_html .= "<option value='" . $p->xaid . "'>.$p->name_xaphuong.</option>";
        }
        return $province_html;

    }
    public function getViewCategory($id){
        if(!Session::get('admin_id') &&!Session::get('id_shop_user')){
            return redirect('admin');

        }

        $checkshop=Shop::where('id_shop_user',Session::get('id_shop_user'))->first();
        if($checkshop->verifyKYC!='Y'){
            Session::put('message_error_kyc','Shop chưa được active!');
            return  back();
        }

        $data=Category_product::all();
        $brands=Brands_product::all();
        $product=Product::find($id);

        return view('admin.product.viewCategory',compact('data','product','brands'));



    }

    public function getListCategory(){
        if(!Session::get('admin_id') &&!Session::get('id_shop_user')){
            return redirect('admin');

        }

        $data=Feeship::orderby('Id_fee',"DESC")->get();
        $fee_html='';
        $fee_html.="<div class='table-responsive'>

<table class='table table-bordered'>
    <thead>
        <tr>
            <th>Tên thành phố </th>
            <th>Tên quận huyện</th>
            <th>Tên xã phường</th>
            <th>Phí ship</th>
</tr>

</thead>
<tbody>
";
        foreach ($data as $d) {
            $fee_html .= "
<tr>
<td>" . $d->city->name_city . "</td>
<td>" . $d->province->name_quanhuyen . "</td>
<td>" . $d->wards->name_xaphuong . "</td>
<td contenteditable class='fee' data-feeship_id='" . $d->Id_fee . "'>" . number_format($d->fee_fee_ship, 0, ',', '.') . "</td>
</tr>";
        }
            $fee_html .= "
</tbody>
</table>
</div>";

return $fee_html;

    }
    public function getFee(Request $request){
        $fee=Feeship::where('fee_xaid',$request->wards)->first();
        if($fee!=null) {
            $free=$fee->fee_fee_ship;
        }else{
            $free=0;
        }    $total = round(Cart::subtotal());
            $total_fee = ($total * 1000) + $free;

            Session::put('feeship', $total_fee);
            Session::put('fee', $fee->fee_fee_ship);
            $fees = number_format($fee->fee_fee_ship, 0, ',', '.');
            $to = number_format($total_fee, 0, ',', '.') . " VNĐ-" . $fees;

        return $to;
    }
    public function getUnactive($id){
        if(!Session::get('admin_id')){
            return redirect('admin');

        }

        $data=Category_product::find($id);
        $data->category_status=0;
        $data->save();
        Session::put('message_success','Cập nhật thành công');
        return redirect('category/list');



    }
    public function getActive($id){
        if(!Session::get('admin_id') &&!Session::get('id_shop_user')){
            return redirect('admin');

        }

        $data=Category_product::find($id);
        $data->category_status=1;
        $data->save();
        Session::put('message_success','Cập nhật thành công');
        return redirect('category/list');



    }
    public function postAddCategory(Request $request){
        if(!Session::get('admin_id') &&!Session::get('id_shop_user')){
            return redirect('admin');

        }

        $checkshop=Shop::where('id_shop_user',Session::get('id_shop_user'))->first();
        if($checkshop->verifyKYC!='Y'){
            Session::put('message_error_kyc','Shop chưa được active!');
            return  back();
        }

        $dheck=Product::where('product_name',$request->product_name)->first();
        if($dheck==null){
            if($request->product_price<=$request->product_price_km){
                Session::put('message_error','Giá sản phẩm khuyến mãi lớn hơn giá sản phẩm !Cập nhật không thành công');
                Session::put('product_code',$request->product_code);
                Session::put('product_name',$request->product_name);
                Session::put('product_price',$request->product_price);
                Session::put('product_desc',$request->product_desc);
                Session::put('product_content',$request->product_content);

                return back();
            }
            $category_product=new Product();

            $category_product->code_product=$request->product_code;
            $category_product->product_name=$request->product_name;
            $category_product->product_name_slug=slugify($request->product_name);
            $category_product->product_desc=$request->product_desc;
            $category_product->product_content=$request->product_content;
            $category_product->category_id=$request->category_id;
            $category_product->brands_id=$request->brands_id;
            $category_product->product_price=$request->product_price;
            $category_product->product_price_km=$request->product_price_km;


            if($request->hasFile('product_images')){
                $file=$request->product_images->getClientOriginalName();
                $rand=generateRandomString(20);
                $day=date('Y-m-d');
                $file_custom=$day.'_'.$rand.'_'.$file;
                $category_product->product_images=$file_custom;
                $request->product_images->move('upload/product/',$file_custom);
            }
            if($request->product_status!=null) {
                $category_product->product_status = $request->product_status;
            }else{
                $category_product->product_status = 0;
            }
            $category_product->save();

            $group_images=$request->product_images_group;

            if($group_images==null){
                $content = new Product_images();
                $content->id_product=$category_product->id_product;
                $content->images='thumbnail_default300x300.png';
                $content->save();

            }else{
                foreach($request->file('product_images_group') as $image) {

                    $content = new Product_images();
                    $content->id_product=$category_product->id_product;
                    $file=$image->getClientOriginalName();
                    $rand=generateRandomString(10);
                    $day=date('Y-m-d');
                    $file_custom=$day.'_'.$rand.'_'.$file;
                    $content->images=$file_custom;
                    $image->move('upload/product/gallyery/',$file_custom);
                    $content->save();
                }
            }
            $product_shop=new Product_shop();
            $product_shop->id_shop=Session::get('id_shop_user');
            $product_shop->id_product=$category_product->id_product;
            $product_shop->status=1;
            $product_shop->save();





            Session::put('message','Cập nhật thành công');
            return redirect('admin/product/list');

        }else{
            Session::put('message_error','Tên sản phẩm đã tồn tại !Cập nhật không thành công');
            Session::put('product_code',$request->product_code);
            Session::put('product_name',$request->product_name);
            Session::put('product_price',$request->product_price);
            Session::put('product_price_km',$request->product_price_km);
            Session::put('product_desc',$request->product_desc);
            Session::put('product_content',$request->product_content);

            return back();
        }
    }
    public function postEditCategory(Request $request){
        if(!Session::get('admin_id') &&!Session::get('id_shop_user')){

            return redirect('admin');

        }
        $fee_value=($request->fee_value);
        $feeship_id=$request->feeship_id;

        $fee_ship= Feeship::find($feeship_id);
        $fee_ship->fee_fee_ship=$fee_value;
        $fee_ship->save();


    }

    public function getDelCategory($id){
        if(!Session::get('admin_id')&& !Session::get('id_shop_user')){
            return redirect('admin');

        }

        $checkshop=Shop::where('id_shop_user',Session::get('id_shop_user'))->first();
        if($checkshop->verifyKYC!='Y'){
            Session::put('message_error_kyc','Shop chưa được active!');
            return  back();
        }

        $check=Category_product::where('id_category_product',$id)->first();
        if($check!=null){
            $check=Category_product::where('category_parent',$check->id_category_product)->get();
            if(count($check)==0){

                Category_product::destroy($id);

                Session::put('message_success','Cập nhật thành công');
                return redirect('admin/product/list');

            }else{

                Session::put('message_error','Danh mục vừa xóa có danh mục con. Cập nhật không thành công');
                return redirect('admin/product/list');

            }

        }
//      Category_product::destroy($id);

//        Session::put('message_success','Cập nhật thành công');
//        return redirect('category/list');


    }
    public function getLogout(){

        Session::put('admin_name','');
        Session::put('admin_id','');
        return redirect('admin');



    }

    public function postDashboard(Request $request){
        $email=$request->Email;
        $password=$request->Password;
        $result=DB::table('tbl_admin')->where('admin_email',$email)->where('admin_password',md5($password))->first();

        if($result!=null){
            Session::put('admin_name',$result->admin_name);
            Session::put('admin_id',$result->id_admin);
            return redirect('admin/dashboard');
        }else{
            Session::put('message',"Lỗi tài khoản hoặc mật khẩu chưa đúng");
            return redirect('admin');
        }



    }

}

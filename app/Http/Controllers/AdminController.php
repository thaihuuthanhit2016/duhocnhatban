<?php
namespace App\Http\Controllers;

date_default_timezone_set("Asia/Bangkok");


use App\Models\Card_user;
use App\Models\City;
use App\Models\Comment;
use App\Models\Config;
use App\Models\Customer;
use App\Models\Product;
use App\Models\Product_shop;
use App\Models\Shipper;
use App\Models\Shop;
use App\Models\Statistical;
use App\Models\Team;
use App\Models\Vistors;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Redirect;
use Session;
class AdminController extends Controller
{



    public function postsetting($id,Request $request){
        $p=Config::find($id);
        $p->title_website=$request->product_name;
        if($request->hasFile('product_images')){

            $file=$request->product_images->getClientOriginalName();
            $rand=generateRandomString(20);
            $day=date('Y-m-d');
            $file_custom=$day.'_'.$rand.'_'.$file;
            $p->logo_header=$file_custom;
            $request->product_images->move('upload/',$file_custom);
        }
        $p->save();
        return back();
    }
    public function getsetting($id){
        $product=Config::find($id);
        return view('admin.setting.editCategory',compact('product'));
    }
    public function getFilter(Request $request){
        $dauthangnay=Carbon::now('Asia/Ho_Chi_Minh')->startOfMonth()->toDateString();
        $dauthangtruoc=Carbon::now('Asia/Ho_Chi_Minh')->subMonth()->startOfMonth()->toDateString();
        $cuoithangtruoc=Carbon::now('Asia/Ho_Chi_Minh')->subMonth()->endOfMonth()->toDateString();

        $sub7days=Carbon::now('Asia/Ho_Chi_Minh')->subDay(7)->toDateString();
        $sub365days=Carbon::now('Asia/Ho_Chi_Minh')->subDay(365)->toDateString();
        $now=Carbon::now('Asia/Ho_Chi_Minh')->toDateString();

        if($request->dashboard_value=='7ngay'){
            $get=Statistical::whereBetween('order_date',[$sub7days,$now])->orderby('order_date',"ASC")->get();
        }elseif ($request->dashboard_value=='thangtruoc'){
            $get=Statistical::whereBetween('order_date',[$dauthangtruoc,$cuoithangtruoc])->orderby('order_date',"ASC")->get();
        }elseif ($request->dashboard_value=='thangnay'){
            $get=Statistical::whereBetween('order_date',[$dauthangnay,$now])->orderby('order_date',"ASC")->get();

        }else{
            $get=Statistical::whereBetween('order_date',[$sub365days,$now])->orderby('order_date',"ASC")->get();
        }
        if(count($get)>0) {
            foreach ($get as $g) {
                $chart_data[] = array(
                    'period' => $g->order_date,
                    'order' => $g->total_order,
                    'sales' => $g->sales,
                    'profit' => $g->profit,
                    'quantity' => $g->quantity

                );
            }
            echo $data = json_encode($chart_data);
        }

        }

    public function get30day(Request $request){
        $sub30days=Carbon::now('Asia/Ho_Chi_Minh')->subDay(30)->toDateString();

        $now=Carbon::now('Asia/Ho_Chi_Minh')->toDateString();
        if(!Session::get('admin_id')) {
            $get = Statistical::whereBetween('order_date', [$sub30days, $now])->where('id_shop', Session::get('id_shop_user'))->orderby("order_date", "ASC")->get();
        }else{
            $get = Statistical::whereBetween('order_date', [$sub30days, $now])->orderby("order_date", "ASC")->get();
        }

        foreach ($get as $g){
            $chart_data[]=array(
                'period'=>$g->order_date,
                'order'=>$g->total_order,
                'sales'=>$g->sales,
                'profit'=>$g->profit,
                'quantity'=>$g->quantity

            );
        }
        echo $data=json_encode($chart_data);
    }
    public function getFilter_by_date(Request $request){

        $from_date=$request->from_date;
        $to_date=$request->to_date;

        $get=Statistical::whereBetween('order_date',[$from_date,$to_date])->orderby("order_date","ASC")->get();

        foreach ($get as $g){
            $chart_data[]=array(
                'period'=>$g->order_date,
                'order'=>$g->total_order,
                'sales'=>$g->sales,
                'profit'=>$g->profit,
                'quantity'=>$g->quantity

            );
        }
        echo $data=json_encode($chart_data);
    }
    public function index(Request $request)
    {
        return view('admin.login');



    }

    public function getApicoin(){
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 0,
            CURLOPT_URL => 'http://localhost/coindemo_1/api/coinsjc',
            CURLOPT_SSL_VERIFYPEER => false
        ));

        $resp = curl_exec($curl);

//Kết quả trả tìm kiếm trả về dạng JSON
        $weather = json_decode($resp);

        print_r($resp); // dump kết quả

        curl_close($curl);
    }
    public function getsupport(Request $request){
        $team=DB::table('tbl_admin')->get();
        return view('admin.chat.index',compact('team'));
    }
    public function getUser(Request $request){
        if(Session::get('id_shop_user')) {
            $team = DB::table('tbl_admin')->where('isOnline', 1)->get();

            $html = '';
            if (count($team) == 0) {
                $html .= 'No user';
            } elseif (count($team) > 0) {
                foreach ($team as $t) {
                    $html .= ' <div class="col-md-6">
                                            <div class="img_">
                                                <img src="http://localhost/shopbitcoin/upload/avatar/user.jpg">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="col-md-9">
                                                <a href="http://localhost/shopbitcoin/chat/';$html.=$t->id_admin;$html.='"> <p>';
                    $html .= $t->admin_name;
                    $html .= '</p>
                                                <span>sss</span>
                                                </a>
                                            </div>
                                            <div class="col-md-3">
                                                <i  class="fa fa-circle" style="color: green;
    margin-top: 6px;"></i>
                                            </div>
                                        </div>';
                }
            }

            return $html;
        }
    }
    public function getChat(Request $request){

        return view('admin.chat.chat');
    }

    public function postsupport(Request $request){
        $team=new Team();
        if(Session::get('id_shop_user'))
        $team->id_shop=Session::get('id_shop_user');
        else{
            $team->id_shop=0;

        }

        if(Session::get('admin_id'))
        $team->id_admin=Session::get('admin_id');
        else{
            $team->id_admin=0;

        }
        $team->message=$request->message;
        $team->id_parent=0;
        $team->created=date('d-m-Y H:i');
        $team->grouped=1;
        $team->save();
        return back();



    }
    public function getMesseage(Request $request,$id){
        if(Session::get('id_shop_user')) {

            $user = DB::table('tbl_admin')->where('id_admin',$id)->first();
            $shop = DB::table('tbl_shop')->where('id_shop_user',Session::get('id_shop_user'))->first();

            Session::put('id_team','Y');
            Session::put('name_support',$user->admin_name);
            Session::put('id_support',$user->id_admin);
            if($shop->name_shop!=null){
                Session::put('name_shop',$shop->name_shop);
            }else{
                Session::put('name_shop',$shop->username);
            }
            return back();

        }else{
            $shop=Shop::where('id_shop_user',$id)->first();
            Session::put('id_shop_user_all',$shop->id_shop_user);
            return back();
        }
    }
    public function showDashboard(Request $request){
        if(!Session::get('admin_id')&&!Session::get('username')){
            return redirect('admin');

        }
        $x="['16-08-2021','17-08-2021','18-08-2021','19-08-2021','20-08-2021','21-08-2021']";
        $sorned=DB::table('test')->select('sorned')->orderby('x','ASC')->get();
        $sorned=json_encode($sorned);

        $tach=str_replace('{"sorned":','',$sorned);
        $sorned=str_replace('}','',$tach);

        $user_ip_address = $request->ip();
        $early_lasth_month = Carbon::now('Asia/Ho_Chi_Minh')->subMonth()->startOfMonth()->toDateString();
        $end_of_lasth_month = Carbon::now('Asia/Ho_Chi_Minh')->subMonth()->endOfMonth()->toDateString();
        $eraly_this_month = Carbon::now('Asia/Ho_Chi_Minh')->startOfMonth()->toDateString();

        $oneyears = Carbon::now('Asia/Ho_Chi_Minh')->subDay(365)->toDateString();
        $now = Carbon::now('Asia/Ho_Chi_Minh')->toDateString();

        $vitors_of_lastMonth = Vistors::whereBetween('date_vistors', [$early_lasth_month, $end_of_lasth_month])->get();
        $vitors_of_lastMonth_count = $vitors_of_lastMonth->count();

        $vitors_of_thisMonth = Vistors::whereBetween('date_vistors', [$eraly_this_month, $now])->get();

        $vitors_of_thisMonth_count = $vitors_of_thisMonth->count();

        $vitors_of_year = Vistors::whereBetween('date_vistors', [$oneyears, $now])->get();
        $vitors_of_year_count = $vitors_of_year->count();

        $vitors_current = Vistors::where('id_address', $user_ip_address)->get();
        $vitors_count = $vitors_current->count();

        if ($vitors_count < 1) {
            $vitors = new Vistors();
            $vitors->id_address = $user_ip_address;
            $vitors->date_vistors = date('Y-m-d');
            $vitors->save();


        }

        $vitors_all=Vistors::all();
        $vitors_total=$vitors_all->count();
        $product_shop=Product_shop::where('id_shop',Session::get('id_shop_user'))->get();
        $product=0;
        foreach ($product_shop as $p_s) {
            $product++;
        }
        if (!Session::get('admin_id')) {
            $sales_vnd = DB::table('tbl_statistical')->where('id_shop', Session::get('id_shop_user'))->where('type', 1)->sum('profit');
            $sales_rut_vnd = DB::table('tbl_card_user')->sum('amount_vnd');

            $sales_vnd=$sales_vnd-$sales_rut_vnd;

            $sales_coin = DB::table('tbl_statistical')->where('id_shop', Session::get('id_shop_user'))->where('type', 3)->sum('profit');
            $sales_rut_coin = DB::table('tbl_card_user')->sum('amount_coin');
            $sales_coin=$sales_coin-$sales_rut_coin;
        }else{

            $sales_vnd = DB::table('tbl_statistical')->where('type', 1)->sum('profit');

            $sales_coin = DB::table('tbl_statistical')->where('type', 3)->sum('profit');
            $sales_rut_coin = DB::table('tbl_card_user')->sum('amount_vnd');
            $sales_coin=$sales_coin-$sales_rut_coin;
        }
        if(Session::get('admin_id')) {
            $account = Customer::where('verifyKYC', 'Y')->get()->count();
            $account_u = Customer::where('verifyKYC', 'N')->get()->count();
            $account_p = Customer::where('verifyKYC', 'P')->get()->count();
            $account_r = Customer::where('verifyKYC', 'R')->get()->count();
        }else{

            $account =0;
            $account_u = 0;
            $account_p = 0;
            $account_r = 0;
        }


        return view('admin.index',compact('x','sorned','account','account_u','account_r','account_p','sales_vnd','sales_coin','product','vitors_total','vitors_count','vitors_of_lastMonth_count','vitors_of_thisMonth_count','vitors_of_year_count'));



    }
    public function approveComment($id){
        $comment=Comment::find($id);
        $comment->comment_status=1;
        $comment->save();
        return \redirect('admin/comment/list');


    }
    public function rejectComment($id){
        $comment=Comment::find($id);
        $comment->comment_status=0;
        $comment->save();
        return \redirect('admin/comment/list');


    }
    public function listComment(){
        $comment=Comment::orderby('id_comments',"DESC")->paginate(10);
        return view('admin.comment.listCategory',compact('comment'));
    }
    public function infoShop()
    {

        $shop=Shop::where('id_shop_user',Session::get('id_shop_user'))->first();
        if(Session::get('username')==null&&Session::get('id_shop_user')==null){
            return redirect('admin');
        }
        $d=Statistical::orderby('order_date',"DESC")->first();
        $day=date('Y-m-d');
        $today = $d->order_date;

        $week = strtotime(date("Y-m-d", strtotime($today)) . " +1 day");
        $week = strftime("%Y-%m-%d", $week);
        if(strtotime($day)>strtotime($week)){
            $shop_all=Shop::all();
            foreach ($shop_all as $s) {
                $Statistical = new Statistical();
                $Statistical->order_date = $week;
                $Statistical->sales = 0;
                $Statistical->profit = 0;
                $Statistical->quantity = 0;
                $Statistical->total_order = 0;
                $Statistical->id_shop = $s->id_shop_user;
                $Statistical->type = 0;
                $Statistical->save();

            }
        }
        return view('admin.shop.infoshop',compact('shop'));

    }
    public function getLogout(){

        Session::put('admin_name','');
        Session::put('admin_id','');
        Session::put('shipper','');
        Session::put('username','');
        Session::put('id_shop_user','');
        return redirect('admin');



    }

    public function postAddCategory(Request $request){
        $shop=Shop::find(Session::get('id_shop_user'));
        if($request->hasFile('images1')){

            $file=$request->images1->getClientOriginalName();
            $rand=generateRandomString(20);
            $day=date('Y-m-d');
            $file_custom=$day.'_'.$rand.'_'.$file;
            $shop->images1=$file_custom;
            $request->images1->move('upload/shop/',$file_custom);
        }
        if($request->hasFile('images2')){
            $file=$request->images2->getClientOriginalName();
            $rand=generateRandomString(20);
            $day=date('Y-m-d');
            $file_custom=$day.'_'.$rand.'_'.$file;
            $shop->images2=$file_custom;
            $request->images2->move('upload/shop/',$file_custom);
        }
        if($request->hasFile('images3')){
            $file=$request->images3->getClientOriginalName();
            $rand=generateRandomString(20);
            $day=date('Y-m-d');
            $file_custom=$day.'_'.$rand.'_'.$file;
            $shop->images3=$file_custom;
            $request->images3->move('upload/shop/',$file_custom);
        }
        $shop->address=$request->address;
        $shop->verifyKYC='P';
        $shop->name_shop=$request->name_shop;
        $shop->save();
        Session::put('message_kyc',"Cập nhật KYC thành công");
        return back();

    }
    public function postShipper(Request $request){
        $shop=Shipper::find(Session::get('admin_id'));
        if($request->hasFile('images1')){

            $file=$request->images1->getClientOriginalName();
            $rand=generateRandomString(20);
            $day=date('Y-m-d');
            $file_custom=$day.'_'.$rand.'_'.$file;
            $shop->images1=$file_custom;
            $request->images1->move('upload/shop/',$file_custom);
        }
        if($request->hasFile('images2')){
            $file=$request->images2->getClientOriginalName();
            $rand=generateRandomString(20);
            $day=date('Y-m-d');
            $file_custom=$day.'_'.$rand.'_'.$file;
            $shop->images2=$file_custom;
            $request->images2->move('upload/shop/',$file_custom);
        }
        if($request->hasFile('images3')){
            $file=$request->images3->getClientOriginalName();
            $rand=generateRandomString(20);
            $day=date('Y-m-d');
            $file_custom=$day.'_'.$rand.'_'.$file;
            $shop->images3=$file_custom;
            $request->images3->move('upload/shop/',$file_custom);
        }
        $shop->address=$request->address;
        $shop->verifyKYC='P';
        $shop->save();
        Session::put('message_kyc',"Cập nhật KYC thành công");
        return back();

    }
    public function getCard(Request $request){
        $card=Card_user::where('status',0)->orderby('id_card_user','DESC')->paginate(10);
        return view('admin.card.listCategory',compact('card'));
    }
    public function editCard($id){
        $card=Card_user::find($id);
        return view('admin.card.editCard',compact('card'));
    }
    public function infoShipper(Request $request){
        $shipper=Shipper::where('id_shipper',Session::get('admin_id'))->first();
        $city=City::orderby('matp','ASC')->get();
        return view('admin.shipper.infoshipper',compact('shipper','city'));
    }
    public function posteditCard(Request $request,$id){
        $card=Card_user::find($id);
        $card->status=$request->status;
        $card->save();
        return redirect('admin/card/list');
    }
    public function postDashboard(Request $request){
            $email=$request->Email;
            $password=$request->Password;

                $result = DB::table('tbl_admin')->where('admin_email', $email)->where('admin_password', md5($password))->first();

                if ($result != null) {
                    Session::put('admin_name', $result->admin_name);
                    Session::put('admin_id', $result->id_admin);
                    return redirect('admin/dashboard');
                } else {
                    Session::put('message', "Lỗi tài khoản hoặc mật khẩu chưa đúng");
                    return redirect('admin');
                }


    }

}

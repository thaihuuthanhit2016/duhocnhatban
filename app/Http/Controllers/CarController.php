<?php
namespace App\Http\Controllers;

date_default_timezone_set("Asia/Bangkok");


use App\Exports\ExcelExports;
use App\Imports\Imports;
use App\Models\Brands_product;
use App\Models\Brands_product_shop;
use App\Models\Car;
use App\Models\Category_product;
use App\Models\Category_product_shop;
use App\Models\Depot;
use App\Models\Product;
use App\Models\Product_images;
use App\Models\Product_shop;
use App\Models\Shop;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB;
use Excel;
use Session;
class CarController extends Controller
{



    public function getAddCategory(){
        if(!Session::get('admin_id')&&!Session::get('username')){
            return redirect('admin');

        }
        $checkshop=Shop::where('id_shop_user',Session::get('id_shop_user'))->first();


        $data=Category_product_shop::where('id_shop',Session::get('id_shop_user'))->get();
        $brands=Brands_product_shop::where('id_shop',Session::get('id_shop_user'))->get();
       return view('admin.car.addCategory',compact('data','brands'));



    }
    public function getEditCategory($id){
        if(!Session::get('admin_id') &&!Session::get('id_shop_user')){
            return redirect('admin');

        }

        $checkshop=Shop::where('id_shop_user',Session::get('id_shop_user'))->first();


        $data=Category_product::all();
        $brands=Brands_product::all();
        $product=Product::find($id);

       return view('admin.car.editCategory',compact('data','product','brands'));



    }

    public function getViewCategory($id){
        if(!Session::get('admin_id') &&!Session::get('id_shop_user')){
            return redirect('admin');

        }

        $checkshop=Shop::where('id_shop_user',Session::get('id_shop_user'))->first();


        $data=Category_product::all();
        $brands=Brands_product::all();
        $product=Car::find($id);

       return view('admin.car.viewCategory',compact('data','product','brands'));



    }

    public function getListCategory(){
        if(!Session::get('admin_id') &&!Session::get('id_shop_user')){
            return redirect('admin');

        }



        $data=Car::paginate(10);
       return view('admin.car.listCategory',compact('data'));



    }
    public function getListCategory_(Request $request){
        if(!Session::get('admin_id') &&!Session::get('id_shop_user')){
            return redirect('admin');

        }

        $checkshop=Shop::where('id_shop_user',Session::get('id_shop_user'))->first();
        if($checkshop->verifyKYC!='Y'){
            Session::put('message_error_kyc','Shop chưa được active!');
            return  back();
        }

        $data=Product_shop::where('id_shop',Session::get('id_shop_user'))->paginate($request->phantrang);
        $html = '';
        foreach ($data as $c) {


            $d = DB::table("tbl_product")->where("id_product", $c->id_product)->first();
                $category = DB::table("tbl_category_product")->where("id_category_product", $d->category_id)->first();

                $brands = DB::table("tbl_brands_product")->where("id_brands_product", $d->brands_id)->first();
                $html .= '<tr>
                        <td><label class="i-checks m-b-none"><input type="checkbox" name="post[]"><i></i></label></td>
                        <td>';
                $html .= $d->product_name;
                if ($d->product_price_km != 0) {
                    $html .= '<sup style="color:red;">Sale</sup>';
                }
                $html
                    .= '</td>
                        <td><img  src="http://localhost/shopbitcoin/upload/product/';
                $html .= $d->product_images;
                $html .= '" width="50px" ></td>
                        <td>';
                $html .= $d->product_price;
                $html .= '</td>
                        <td>';
                $html .= $d->product_price_km;
                $html .= '</td>
                        <td>';
                if ($d->product_status == 0) {
                    $html .= '<span style="color:red;">Hết hàng </span>';
                } else {
                    $html .= '<span style="color:green;"> Còn hàng </span>';
                }
                $html .= '</td>';
                if ($category != null) {
                    $html .= '<td>';
                    $html .= $category->category_name;
                    $html .= '</td>';
                } else {
                    $html .= '<td>None</td>';
                }
                if ($brands != null) {
                    $html .= '<td>';
                    $html .= $brands->brands_name;
                    $html .= '</td>';
                } else {
                    $html .= '<td>None</td>';
                }
                $html .= '

                        <td>
                            <a href="http://localhost/shopbitcoin/admin/product/view/';
                $html .= $d->id_product;
                $html .= '" class="active" ui-toggle-class=""><i class="fa fa-eye text-success text-active"></i></a>&nbsp;&nbsp;&nbsp;<a href="http://localhost/shopbitcoin/admin/product/edit/';
                $html .= $d->id_product;
                $html .= '" class="active" ui-toggle-class=""><i class="fa fa-pencil-square-o text-success text-active"></i></a>&nbsp;&nbsp;&nbsp; <a href="http://localhost/shopbitcoin/admin/product/del/';
                $html .= $d->id_product;
                $html .= '" onclick="confirm("Bạn có chắc xóa không?")"> <i class="fa fa-times text-danger text"></i></a>
                        </td>
                    </tr>';


            }
        return $html;
        }




    public function serachproduct(Request $request)
    {
        if($request->search!=null) {

            $data = Product::where('product_name', 'LIKE', '%' . $request->search . '%')->get();

            $html = '';

            foreach ($data as $c) {


                $d = DB::table("tbl_product_shop")->where("id_product", $c->id_product)->where("id_shop", Session::get('id_shop_user'))->first();
                if ($d != null) {
                    $category = DB::table("tbl_category_product")->where("id_category_product", $c->category_id)->first();

                    $brands = DB::table("tbl_brands_product")->where("id_brands_product", $c->brands_id)->first();
                    $html .= '<tr>
                        <td><label class="i-checks m-b-none"><input type="checkbox" name="post[]"><i></i></label></td>
                        <td>';
                    $html .= $c->product_name;
                    if ($c->product_price_km != 0) {
                        $html .= '<sup style="color:red;">Sale</sup>';
                    }
                    $html
                        .= '</td>
                        <td><img  src="http://localhost/shopbitcoin/upload/product/';
                    $html .= $c->product_images;
                    $html .= '" width="50px" ></td>
                        <td>';
                    $html .= $c->product_price;
                    $html .= '</td>
                        <td>';
                    $html .= $c->product_price_km;
                    $html .= '</td>
                        <td>';
                    if ($c->product_status == 0) {
                        $html .= '<span style="color:red;">Hết hàng </span>';
                    } else {
                        $html .= '<span style="color:green;"> Còn hàng </span>';
                    }
                    $html .= '</td>';
                    if ($category != null) {
                        $html .= '<td>';
                        $html .= $category->category_name;
                        $html .= '</td>';
                    } else {
                        $html .= '<td>None</td>';
                    }
                    if ($brands != null) {
                        $html .= '<td>';
                        $html .= $brands->brands_name;
                        $html .= '</td>';
                    } else {
                        $html .= '<td>None</td>';
                    }
                    $html .= '

                        <td>
                            <a href="http://localhost/shopbitcoin/admin/product/view/';
                    $html .= $c->id_product;
                    $html .= '" class="active" ui-toggle-class=""><i class="fa fa-eye text-success text-active"></i></a>&nbsp;&nbsp;&nbsp;<a href="http://localhost/shopbitcoin/admin/product/edit/';
                    $html .= $c->id_product;
                    $html .= '" class="active" ui-toggle-class=""><i class="fa fa-pencil-square-o text-success text-active"></i></a>&nbsp;&nbsp;&nbsp; <a href="http://localhost/shopbitcoin/admin/product/del/';
                    $html .= $c->id_product;
                    $html .= '" onclick="confirm("Bạn có chắc xóa không?")"> <i class="fa fa-times text-danger text"></i></a>
                        </td>
                    </tr>';
                }
            }
        }else {

            $data = Product::get();

            $html = '';

            foreach ($data as $c) {


                $d = DB::table("tbl_product_shop")->where("id_product", $c->id_product)->where("id_shop", Session::get('id_shop_user'))->first();
                if ($d != null) {
                    $category = DB::table("tbl_category_product")->where("id_category_product", $c->category_id)->first();

                    $brands = DB::table("tbl_brands_product")->where("id_brands_product", $c->brands_id)->first();
                    $html .= '<tr>
                        <td><label class="i-checks m-b-none"><input type="checkbox" name="post[]"><i></i></label></td>
                        <td>';
                    $html .= $c->product_name;
                    if ($c->product_price_km != 0) {
                        $html .= '<sup style="color:red;">Sale</sup>';
                    }
                    $html
                        .= '</td>
                        <td><img  src="http://localhost/shopbitcoin/upload/product/';
                    $html .= $c->product_images;
                    $html .= '" width="50px" ></td>
                        <td>';
                    $html .= $c->product_price;
                    $html .= '</td>
                        <td>';
                    $html .= $c->product_price_km;
                    $html .= '</td>
                        <td>';
                    if ($c->product_status == 0) {
                        $html .= '<span style="color:red;">Hết hàng </span>';
                    } else {
                        $html .= '<span style="color:green;"> Còn hàng </span>';
                    }
                    $html .= '</td>';
                    if ($category != null) {
                        $html .= '<td>';
                        $html .= $category->category_name;
                        $html .= '</td>';
                    } else {
                        $html .= '<td>None</td>';
                    }
                    if ($brands != null) {
                        $html .= '<td>';
                        $html .= $brands->brands_name;
                        $html .= '</td>';
                    } else {
                        $html .= '<td>None</td>';
                    }
                    $html .= '

                        <td>
                            <a href="http://localhost/shopbitcoin/admin/product/view/';
                    $html .= $c->id_product;
                    $html .= 'class="active" ui-toggle-class=""><i class="fa fa-eye text-success text-active"></i></a>&nbsp;&nbsp;&nbsp;<a href="http://localhost/shopbitcoin/admin/product/edit/';
                    $html .= $c->id_product;
                    $html .= '" class="active" ui-toggle-class=""><i class="fa fa-pencil-square-o text-success text-active"></i></a>&nbsp;&nbsp;&nbsp; <a href="http://localhost/shopbitcoin/admin/product/del/';
                    $html .= $c->id_product;
                    $html .= '" onclick="confirm("Bạn có chắc xóa không?")"> <i class="fa fa-times text-danger text"></i></a>
                        </td>
                    </tr>';
                }
            }
        }
        return $html;
    }
    public function getDelImages($id){
        if(!Session::get('admin_id')&&!Session::get('id_shop_user')){
            return redirect('admin');

        }

        $checkshop=Shop::where('id_shop_user',Session::get('id_shop_user'))->first();
        if($checkshop->verifyKYC!='Y'){
            Session::put('message_error_kyc','Shop chưa được active!');
            return  back();
        }

        Product_images::destroy($id);
        Session::put('message_success','Cập nhật thành công');
        return back();

    }
    public function getUnactive($id){
        if(!Session::get('admin_id')){
            return redirect('admin');

        }

        $data=Category_product::find($id);
        $data->category_status=0;
        $data->save();
        Session::put('message_success','Cập nhật thành công');
       return redirect('category/list');



    }
    public function getActive($id){
        if(!Session::get('admin_id') &&!Session::get('id_shop_user')){
            return redirect('admin');

        }

        $data=Category_product::find($id);
        $data->category_status=1;
        $data->save();
        Session::put('message_success','Cập nhật thành công');
       return redirect('category/list');



    }
    public function postAddCategory(Request $request){
        if(!Session::get('admin_id') &&!Session::get('id_shop_user')){
            return redirect('admin');

        }
        $dheck=Car::where('product_name',$request->product_name)->first();
        if($dheck==null){

            $category_product=new Car();

            $category_product->code_product=$request->product_code;
            $category_product->product_tag=$request->product_tags;
            $category_product->product_name=$request->product_name;
            $category_product->product_name_slug=slugify($request->product_name);
            $category_product->product_desc=$request->product_desc;
            $category_product->product_content=$request->product_content;
            $category_product->category_id=0;
            $category_product->phone=$request->phone;
            $category_product->brands_id=0;
            $category_product->product_price=$request->product_price;
            $category_product->product_price_km=$request->product_price_km;


            if($request->hasFile('product_images')){

                $file=$request->product_images->getClientOriginalName();
                $rand=generateRandomString(20);
                $day=date('Y-m-d');
                $file_custom=$day.'_'.$rand.'_'.$file;
                $category_product->product_images=$file_custom;
                $request->product_images->move('upload/product/',$file_custom);
            }
            if($request->product_status!=null) {
                $category_product->product_status = $request->product_status;
            }else{
                $category_product->product_status = 0;
            }
            $category_product->save();



            Session::put('message','Cập nhật thành công');
            return redirect('admin/car/list');

        }else{
            Session::put('message_error','Tên sản phẩm đã tồn tại !Cập nhật không thành công');
            Session::put('product_code',$request->product_code);
            Session::put('product_name',$request->product_name);
            Session::put('product_price',$request->product_price);
            Session::put('product_price_km',$request->product_price_km);
            Session::put('product_desc',$request->product_desc);
            Session::put('product_content',$request->product_content);

            return back();
        }
    }
    public function postEditCategory(Request $request,$id){
        if(!Session::get('admin_id') &&!Session::get('id_shop_user')){

            return redirect('admin');

        }


        $checkshop=Shop::where('id_shop_user',Session::get('id_shop_user'))->first();


            $category_product= Car::find($id);

            $category_product->product_name=$request->product_name;
            $category_product->product_name_slug=slugify($request->product_name);
            $category_product->product_desc=$request->product_desc;
            $category_product->product_content=$request->product_content;
            $category_product->category_id=$request->category_id;
            $category_product->brands_id=$request->brands_id;
            $category_product->product_price=$request->product_price;
            $category_product->product_price_km=$request->product_price_km;
        $category_product->product_tag=$request->product_tags;
        $category_product->phone=$request->phone;


            if($request->hasFile('product_images')){
                $file=$request->product_images->getClientOriginalName();
                $rand=generateRandomString(20);
                $day=date('Y-m-d');
                $file_custom=$day.'_'.$rand.'_'.$file;
                $category_product->product_images=$file_custom;
                $request->product_images->move('upload/product/',$file_custom);
            }
        $category_product->product_status=1;
            $category_product->save();

            $group_images=$request->product_images_group;

            if($group_images!=null){

                foreach($request->file('product_images_group') as $image) {

                    $content = new Product_images();
                    $content->id_product=$category_product->id_product;
                    $file=$image->getClientOriginalName();
                    $rand=generateRandomString(10);
                    $day=date('Y-m-d');
                    $file_custom=$day.'_'.$rand.'_'.$file;
                    $content->images=$file_custom;
                    $image->move('upload/product/gallyery/',$file_custom);
                    $content->save();
                }
            }


            Session::put('message','Cập nhật thành công');
            return redirect('admin/car/list');



    }

    public function getDelCategory($id){
        if(!Session::get('admin_id')&& !Session::get('id_shop_user')){
            return redirect('admin');

        }

        $checkshop=Shop::where('id_shop_user',Session::get('id_shop_user'))->first();
        if($checkshop->verifyKYC!='Y'){
            Session::put('message_error_kyc','Shop chưa được active!');
            return  back();
        }

        $check=Category_product::where('id_category_product',$id)->first();
        if($check!=null){
            $check=Category_product::where('category_parent',$check->id_category_product)->get();
            if(count($check)==0){

              Category_product::destroy($id);

                Session::put('message_success','Cập nhật thành công');
                return redirect('admin/car/list');

            }else{

                Session::put('message_error','Danh mục vừa xóa có danh mục con. Cập nhật không thành công');
                return redirect('admin/product/list');

            }

        }
//      Category_product::destroy($id);

//        Session::put('message_success','Cập nhật thành công');
//        return redirect('category/list');


    }
    public function getLogout(){

        Session::put('admin_name','');
        Session::put('admin_id','');
        return redirect('admin');



    }

    public function export_csv(){

        return Excel::download(new ExcelExports() , 'product.xlsx');

    }
    public function import_csv(Request $request){

        $path = $request->file('file')->getRealPath();
        Excel::import(new Imports(), $path);
        return back();


    }

}

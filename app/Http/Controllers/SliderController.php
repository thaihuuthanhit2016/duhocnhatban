<?php
namespace App\Http\Controllers;
date_default_timezone_set("Asia/Bangkok");



use App\Models\Brands_product;
use App\Models\Brands_product_shop;
use App\Models\Category_product;
use App\Models\Category_product_shop;
use App\Models\Product;
use App\Models\Product_images;
use App\Models\Product_shop;
use App\Models\Shop;
use App\Models\Slider;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB;
use Session;
class SliderController extends Controller
{



    public function getAddCategory(){
        if(!Session::get('admin_id')){
            return redirect('admin');

        }

        return view('admin.slider.addCategory');



    }
    public function getEditCategory($id){
        if(!Session::get('admin_id')){
            return redirect('admin');

        }


        $data=Category_product::all();
        $brands=Brands_product::all();
        $product=Slider::find($id);


       return view('admin.slider.editCategory',compact('data','product','brands'));



    }

    public function getViewCategory($id){
        if(!Session::get('admin_id')){
            return redirect('admin');

        }

        $checkshop=Shop::where('id_shop_user',Session::get('id_shop_user'))->first();
        if($checkshop->verifyKYC!='Y'){
            Session::put('message_error_kyc','Shop chưa được active!');
            return  back();
        }

        $data=Category_product::all();
        $brands=Brands_product::all();
        $product=Product::find($id);

       return view('admin.product.viewCategory',compact('data','product','brands'));



    }

    public function getListCategory(){
        if(!Session::get('admin_id')){
            return redirect('admin');

        }

        $data=Slider::orderby('id_slide','DESC')->paginate(10);
       return view('admin.slider.listCategory',compact('data'));



    }
    public function getDelImages($id){
        if(!Session::get('admin_id')){
            return redirect('admin');

        }


        Product_images::destroy($id);
        Session::put('message_success','Cập nhật thành công');
        return back();

    }
    public function getUnactive($id){
        if(!Session::get('admin_id')){
            return redirect('admin');

        }

        $data=Category_product::find($id);
        $data->category_status=0;
        $data->save();
        Session::put('message_success','Cập nhật thành công');
       return redirect('slider/list');



    }
    public function getActive($id){
        if(!Session::get('admin_id') &&!Session::get('id_shop_user')){
            return redirect('admin');

        }

        $data=Category_product::find($id);
        $data->category_status=1;
        $data->save();
        Session::put('message_success','Cập nhật thành công');
       return redirect('slider/list');



    }
    public function postAddCategory(Request $request){
        if(!Session::get('admin_id')){
            return redirect('admin');

        }

        $dheck=Slider::where('title',$request->title)->first();
        if($dheck==null) {
            $category_product = new Slider();

            $category_product->title = $request->title;
            $category_product->link = $request->link;
            $category_product->status = 1;

            $category_product->type = $request->loai;
            if ($request->hasFile('images')) {
                $file = $request->images->getClientOriginalName();
                $rand = generateRandomString(20);
                $day = date('Y-m-d');
                $file_custom = $day . '_' . $rand . '_' . $file;
                $category_product->images = $file_custom;
                $request->images->move('upload/slider/', $file_custom);
            }
            $category_product->save();
            return redirect('admin/slider/list');
        }
    }
    public function postEditCategory(Request $request,$id){

        if(!Session::get('admin_id')){
            return redirect('admin');

        }
        $category_product=Slider::find($id);

        $category_product->title = $request->title;
        $category_product->link = $request->link;

        $category_product->type = $request->loai;
        if ($request->hasFile('images')) {
            $file = $request->images->getClientOriginalName();
            $rand = generateRandomString(20);
            $day = date('Y-m-d');
            $file_custom = $day . '_' . $rand . '_' . $file;
            $category_product->images = $file_custom;
            $request->images->move('upload/slider/', $file_custom);
        }
        $category_product->save();
        return redirect('admin/slider/list');
    }

    public function getDelCategory($id){
        if(!Session::get('admin_id')&& !Session::get('id_shop_user')){
            return redirect('admin');

        }

        $checkshop=Shop::where('id_shop_user',Session::get('id_shop_user'))->first();
        if($checkshop->verifyKYC!='Y'){
            Session::put('message_error_kyc','Shop chưa được active!');
            return  back();
        }

        $check=Category_product::where('id_category_product',$id)->first();
        if($check!=null){
            $check=Category_product::where('category_parent',$check->id_category_product)->get();
            if(count($check)==0){

              Category_product::destroy($id);

                Session::put('message_success','Cập nhật thành công');
                return redirect('admin/slider/list');

            }else{

                Session::put('message_error','Danh mục vừa xóa có danh mục con. Cập nhật không thành công');
                return redirect('admin/slider/list');

            }

        }
//      Category_product::destroy($id);

//        Session::put('message_success','Cập nhật thành công');
//        return redirect('category/list');


    }
    public function getLogout(){

        Session::put('admin_name','');
        Session::put('admin_id','');
        return redirect('admin');



    }

    public function postDashboard(Request $request){
            $email=$request->Email;
            $password=$request->Password;
            $result=DB::table('tbl_admin')->where('admin_email',$email)->where('admin_password',md5($password))->first();

            if($result!=null){
    Session::put('admin_name',$result->admin_name);
    Session::put('admin_id',$result->id_admin);
    return redirect('admin/dashboard');
            }else{
                Session::put('message',"Lỗi tài khoản hoặc mật khẩu chưa đúng");
                return redirect('admin');
            }



    }

}

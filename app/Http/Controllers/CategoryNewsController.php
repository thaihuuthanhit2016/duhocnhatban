<?php
namespace App\Http\Controllers;
date_default_timezone_set("Asia/Bangkok");



use App\Models\Category_news;
use App\Models\Category_product;
use App\Models\Category_product_shop;
use App\Models\Shop;
use Illuminate\Http\Request;
use DB;
use Session;
class CategoryNewsController extends Controller
{



    public function getAddCategory(){
        if(!Session::get('admin_id')&&!Session::get('id_shop_user')){
            return redirect('admin');

        }


       return view('admin.category_news.addCategory');



    }
    public function getEditCategory($id){
        if(!Session::get('admin_id')&&!Session::get('id_shop_user')){
            return redirect('admin');

        }
        $aa=Category_news::find($id);

       return view('admin.category_news.editCategory',compact('aa'));



    }

    public function getListCategory(){
        if(!Session::get('admin_id')&&!Session::get('id_shop_user')){
            return redirect('admin');

        }
        $category_shop=Category_news::orderby('id_category_news',"DESC")->paginate(10);

       return view('admin.category_news.listCategory',compact('category_shop'));



    }
    public function postAddCategory(Request $request){
        if(!Session::get('admin_id')&&!Session::get('id_shop_user')){
            return redirect('admin');

        }

        $dheck=Category_news::where('category_name',$request->category_product_name)->first();
        if($dheck==null){

            $category_product=new Category_news();

            $category_product->category_name=$request->category_product_name;
            $category_product->category_name_slug=slugify($request->category_product_name);
            $category_product->category_desc=$request->category_product_desc;
            if($request->category_status!=null) {
                $category_product->category_status = $request->category_status;
            }else{
                $category_product->category_status = 0;
            }
            $category_product->save();


            Session::put('message','Cập nhật thành công');
            return redirect('admin/category-news/list');

        }else{
            Session::put('message_error','Tên danh mục đã tồn tại !Cập nhật không thành công');
            Session::put('name_category',$request->category_product_name);
            Session::put('category_desc',$request->category_product_desc);
            Session::put('parent',$request->parent);

            return back();
        }
    }
    public function postEditCategory(Request $request,$id){
        if(!Session::get('admin_id')&&!Session::get('id_shop_user')){
            return redirect('admin');

        }


            $category_product=Category_news::find($id);

            $category_product->category_name=$request->category_product_name;
            $category_product->category_name_slug=slugify($request->category_product_name);
            $category_product->category_desc=$request->category_product_desc;
            if($request->category_status!=null) {
                $category_product->category_status = $request->category_status;
            }else{
                $category_product->category_status = 0;
            }
            $category_product->save();

            Session::put('message','Cập nhật thành công');
            return redirect('admin/category-news/list');






    }

    public function getDelCategory($id){
        if(!Session::get('admin_id')&&!Session::get('id_shop_user')){
            return redirect('admin');

        }

        $check=Category_news::where('id_shop',Session::get('id_shop_user'))->where('id_category_product',$id)->first();
        if($check!=null){
            $check=Category_product::where('category_parent',$check->id_category_product)->get();
            if(count($check)==0){
        Category_product_shop::where('id_category_product',$id)->delete();
              Category_product::destroy($id);

                Session::put('message_success','Cập nhật thành công');
                return redirect('admin/category-news/list');

            }else{

                Session::put('message_error','Danh mục vừa xóa có danh mục con. Cập nhật không thành công');
                return redirect('category/list');

            }

        }
//      Category_product::destroy($id);

//        Session::put('message_success','Cập nhật thành công');
//        return redirect('category/list');


    }
    public function getLogout(){

        Session::put('admin_name','');
        Session::put('admin_id','');
        return redirect('admin');



    }

    public function postDashboard(Request $request){
            $email=$request->Email;
            $password=$request->Password;
            $result=DB::table('tbl_admin')->where('admin_email',$email)->where('admin_password',md5($password))->first();

            if($result!=null){
    Session::put('admin_name',$result->admin_name);
    Session::put('admin_id',$result->id_admin);
    return redirect('admin/dashboard');
            }else{
                Session::put('message',"Lỗi tài khoản hoặc mật khẩu chưa đúng");
                return redirect('admin');
            }



    }

}

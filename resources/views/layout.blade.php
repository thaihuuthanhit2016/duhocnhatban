<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Du học Nhật Bản</title>
    <link href="{{asset('public/Eshopper/css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link href="{{asset('public/Eshopper/css/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{asset('public/Eshopper/css/price-range.css')}}" rel="stylesheet">
    <link href="{{asset('public/Eshopper/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('public/Eshopper/css/main.css')}}" rel="stylesheet">
    <link href="{{asset('public/Eshopper/css/responsive.css')}}" rel="stylesheet">
    <link href="{{asset('public/Eshopper/css/lightgallery.min.css')}}" rel="stylesheet">
    <link href="{{asset('public/Eshopper/css/lightslider.css')}}" rel="stylesheet">
    <link href="{{asset('public/Eshopper/css/prettify.css')}}" rel="stylesheet">
    <link href="{{asset('public/Eshopper/css/owl.carousel.min.css')}}" rel="stylesheet">


    <link rel='stylesheet' href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css'>
    <script src="{{asset('public/Eshopper/js/html5shiv.js')}}"></script>
    <script src="{{asset('public/Eshopper/js/respond.min.js')}}"></script>
    <![endif]-->
    <link rel="shortcut icon" href="{{asset('public/Eshopper/images/ico/favicon.ico')}}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset('public/Eshopper/images/ico/apple-touch-icon-144-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('public/Eshopper/images/ico/apple-touch-icon-114-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('public/Eshopper/images/ico/apple-touch-icon-72-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" href="{{asset('public/Eshopper/images/ico/apple-touch-icon-57-precomposed.png')}}">
</head><!--/head-->

<body>
<header id="header"><!--header-->
    <div class="header_top"><!--header_top-->
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="contactinfo">
                        <ul class="nav nav-pills">
                            <li><a href="#"><i class="fa fa-phone"></i> +2 95 01 88 821</a></li>
                            <li><a href="#"><i class="fa fa-envelope"></i> info@domain.com</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="social-icons pull-right">
                        <ul class="nav navbar-nav">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header_top-->

    <?php
    $config=DB::table('tbl_config')->first();

    ?>    <div class="header-middle"><!--header-middle-->
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="logo pull-left">
                        <a href="{{asset('/')}}"><img src="{{asset('upload/'.$config->logo_header)}}" alt="" /></a>
                    </div>

                </div>
                <div class="col-sm-8">
                </div>
            </div>
        </div>
    </div><!--/header-middle-->

    <div class="header-bottom"><!--header-bottom-->
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="mainmenu pull-left">
                        <ul class="nav navbar-nav collapse navbar-collapse">
                            <li><a href="{{asset('/')}}" class="active">Trang chủ</a></li>
                            <?Php
                                $gioithieu=DB::table('tbl_pages')->where('type',0)->first();
                            ?>
                            @if($gioithieu!=null)
                            <li class="dropdown"><a href="{{asset('introduce')}}">{{$gioithieu->title}}</a>

                            </li>
                            @endif
                            <li class="dropdown"><a href="{{asset('product')}}">Du học</a>

                            </li>
                            <li class="dropdown"><a href="{{asset('car')}}">Xe</a>

                            </li>
                            <li><a href="{{asset('contact')}}">Liên hệ</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="search_box pull-right">
                        <form method="post" action="{{asset('search')}}">
                            @csrf
                            <div class="row">
                                <div class="col-md-7">
                                    <input  type="text" name="keyword" id="keyword" class="form-control" placeholder="Tìm kiếm sản phẩm"/>
                                    <div id="serach_ajax"></div>
                                </div>
                                <div class="col-md-5">
                                    <input  style="margin-top: 0px;color:white;"type="submit" class="btn btn-primary" value="Tìm"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header-bottom-->
</header><!--/header-->
<div class="clearfix"></div>
<section id="slider"><!--slider-->
    <div id="slider-carousel" class="carousel slide" data-ride="carousel">

        <?php
        $i=0;
        $slider =DB::table('tbl_slider')->where('status',1)->where('type',0)->get();?>

        <ol class="carousel-indicators">
            @for($j=0;$j<count($slider);$j++)
                <li data-target="#slider-carousel" data-slide-to="{{$j}}" class="@if($j==1) active @endif"></li>
            @endfor
        </ol>
        <div class="carousel-inner">
            @foreach($slider as $s)
                <?php $i++;?>
                <div class="item @if($i==1)active @endif" style="padding-left: 0px;">
                    <div class="col-sm-12" style="padding-left: 0px !important;padding-right: 0px !important;">
                        <img src="{{asset('upload/slider/'.$s->images)}}"   alt="" width="100%" height="300px"/>
                    </div>
                </div>
            @endforeach
        </div>

        <a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
            <i class="fa fa-angle-left"></i>
        </a>
        <a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
            <i class="fa fa-angle-right"></i>
        </a>
    </div>
</section><!--/slider-->

<section>
    <div class="container">
        <div class="row">

            <div class="col-sm-12 padding-right">
                @yield('content')
            </div>
        </div>
    </div>

</section>
@include('template.footer')

<script src="{{asset('public/Eshopper/js/jquery.js')}}"></script>
<script src="{{asset('public/Eshopper/js/bootstrap.min.js')}}"></script>
<script src="{{asset('public/Eshopper/js/jquery.scrollUp.min.js')}}"></script>
<script src="{{asset('public/Eshopper/js/price-range.js')}}"></script>
<script src="{{asset('public/Eshopper/js/jquery.prettyPhoto.js')}}"></script>
<script src="{{asset('public/Eshopper/js/main.js')}}"></script>
<script src="{{asset('public/Eshopper/js/lightslider.js')}}"></script>
<script src="{{asset('public/Eshopper/js/lightgallery-all.min.js')}}"></script>
<script src="{{asset('public/Eshopper/js/owl.carousel.js')}}"></script>
<script src="{{asset('public/Eshopper/js/prettify.js')}}"></script>
<script src='https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script>
</body>
</html>
<script>
    function  remove_background(product_id){
        for(var count=1;count<=5;count++){
            $("#"+product_id+'-'+count).css('color','#ccc');
        }
    }
    $(document).on('mouseenter','.rating',function (){
        var index =$(this).data('index');
        var product_id=$(this).data('product_id');

        remove_background(product_id);
        for(var count=1;count<=index;count++){
            $("#"+product_id+'-'+count).css('color','#ffcc00');
        }
    });
    $(document).on('mouseleave','.rating',function (){
        var index =$(this).data('index');
        var product_id=$(this).data('product_id');
        var rating=$(this).data('rating');
        remove_background(product_id);
        for(var count=1;count<=rating;count++){
            $("#"+product_id+'-'+count).css('color','#ffcc00');
        }
    });
    $(document).on('click','.rating',function (){
        var index =$(this).data('index');
        var product_id=$(this).data('product_id');
        $.ajax({
            url:"{{asset('rating')}}",
            method:'get',
            data:{index:index,product_id:product_id},
            success:function (data){
                if(data=="done"){
                    alert("Bạn đã đánh giá "+index+" trên 5");
                }else {
                    alert("Lỗi đánh giá");
                }
            }
        })
    });
    $(document).ready(function() {


        $("#keyword").keyup(function (){
            var query =$(this).val();
            if(query!=''){
                $.ajax({
                    url:'{{asset('autocomplect_ajax_search')}}',
                    method:"get",
                    data:{query:query},
                    success:function (data){
                        $("#serach_ajax").fadeIn();
                        $("#serach_ajax").html(data);

                    }
                });
            }else {
                $("#serach_ajax").fadeOut();

            }

        });
        $(document).on('click','li',function (){
            $("#keyword").val($(this).text());
            $("#serach_ajax").fadeOut();

        });
        $("#price-range").slider({range: true, min: 0, max: 200000, values: [0, 200000], slide: function(event, ui) {$("#priceRange").val("$" + ui.values[0] + " - $" + ui.values[1]);}
        });
        $("#priceRange").val("$" + $("#price-range").slider("values", 0) + " - $" + $("#price-range").slider("values", 1));



        $('#imageGallery').lightSlider({
            gallery:true,
            item:1,
            loop:true,
            thumbItem:9,
            slideMargin:0,
            enableDrag: false,
            currentPagerPosition:'left',
            onSliderLoad: function(el) {
                el.lightGallery({
                    selector: '#imageGallery .lslide'
                });
            }
        });
        $('#sort').on('change',function (){
            var url=$(this).val();
            if(url){
                window.location=url;
            }
            return false;
        });

    });
    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:true,

        autoplay:true,
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:3
            }
        }
    })

</script>
<style>
    button.owl-prev {
        margin-left: -320px;
        margin-top: -144px;
        position: absolute;
    }
    button.owl-next {
        margin-left: 310px;
        margin-top: -144px;
        position: absolute;
    }
</style>

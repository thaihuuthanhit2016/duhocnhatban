<!DOCTYPE html>
<html lang="en">
@include('template.header.head_cart')

<body>
@include('template.header.cart')
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                @include('pages.left_menu')
            </div>

            <div class="col-sm-9">
                    <h3 class="title">DANH SÁCH SẢN PHẨM SO SÁNH  CỦA BẠN</h3>
                <br>
                <div class="table-responsive cart_info">
                    <table class="table table-condensed" border="1">
                        <thead>
                        <?php
                        $addressvi="lGAJDaThG28Y7UtGTU7jJ1apRLczA2PfdUUSw1";
                        $i=0;
                        ?>
                        @foreach($product_compare as $b)

                                <?Php
                                    $product=DB::table('tbl_product')->where('id_product',$b->id_product)->first();
                                ?>


                                    <tr class="cart_menu">
                            <td class="image" style="width: 100px;">Mã sản phẩm:<br><b> {{$product->code_product}}</b></td>
                            <td class="image" style="width: 100px;">Ảnh sản phẩm:<br><img width="100px" src="{{asset('upload/product/'.$product->product_images)}}"></td>

                            <td class="price" style="width: 120px">Tên sản phẩm: <br> <b>{{$product->product_name}}</b></td>
                            <td class="quantity" style="width: 40px">Giá:<br><b> @if($product->product_price_km!=0){{number_format($product->product_price_km,0,',','.')}} @else {{number_format($product->product_price,0,',','.')}} @endif VNĐ</b></td>
                                        <td class="quantity" style="width: 40px"><a  style="color: #FE980F" href="{{asset('add-to-cart/'.$product->id_product)}}" >Mua ngay</a></td>


                        </tr>


                            @endforeach
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</section>

@include('template.footer')
<style>
    .active{
        color: #FE980F!important;
    }
    td{
        text-align: center;
    }
</style>

<script>
    $(document).ready(function (){
        $('#order').addClass('active');
    });
</script>
<?php
$i=0;
?>
<script>
    function  remove_background(product_id){
        for(var count=1;count<=5;count++){
            $("#"+product_id+'-'+count).css('color','#ccc');
        }
    }

    $(document).on('mouseenter','.rating',function (){
        var index =$(this).data('index');
        var product_id=$(this).data('product_id');

        remove_background(product_id);
        for(var count=1;count<=index;count++){
            $("#"+product_id+'-'+count).css('color','#ffcc00');
        }
    });
    $(document).on('mouseleave','.rating',function (){
        var index =$(this).data('index');
        var product_id=$(this).data('product_id');
        var rating=$(this).data('rating');
        remove_background(product_id);
        for(var count=1;count<=rating;count++){
            $("#"+product_id+'-'+count).css('color','#ffcc00');
        }
    });
    $(document).on('click','.rating',function (){
        var index =$(this).data('index');
        var product_id=$(this).data('product_id');
        $.ajax({
            url:"{{asset('rating')}}",
            method:'get',
            data:{index:index,product_id:product_id},
            success:function (data){
                if(data=="done"){
                    location.reload();
                }else {
                    alert("Lỗi đánh giá");
                }
            }
        })
    });
</script>

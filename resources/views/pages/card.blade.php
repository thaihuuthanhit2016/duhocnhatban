@extends('layout')
@section('content')
    <div class="features_items"><!--features_items-->
        <h2 class="title text-center">Card</h2>
        @include('admin.errors.error')
        <br>
        <div class="clearfix"></div>
        <br>
        <div><h3  class="title-oo">Card Deposit</h3></div>
        <div class="row">
            @foreach($card as $o)
                <div class="col-md-4">
                    <div class="box" style="margin-bottom: 15px">
                        <div class="row">
                            <div  class="col-md-12">
                                <p class="title_1">Seri: <b>{{$o->seri}}</b></p>
                                <p class="title_1">Code: <b> {{$o->code_card}}</b></p>
                                <p class="title_1">USD: <b>$ {{number_format($o->usd,0,',','.')}}</b></p>
                                <p class="title_1">VNĐ: <b> {{number_format($o->usd*20000,0,',','.')}} VNĐ</b></p>
                                <p class="title_1">Coin:  <b>{{number_format($o->coin,0,',','.')}}</b></p>
                                <a class="btn btn-primary" style="width: 100%;text-align: center" href="{{asset('payment/buycard/'.$o->id_card)}}">Mua ngay</a>

                            </div>
                        </div>

                    </div>
                </div>
            @endforeach
        </div>


@endsection
<style>
    .title-oo{
        padding: 10px;
        color: white;
        background: #FE980F;
    }
</style>

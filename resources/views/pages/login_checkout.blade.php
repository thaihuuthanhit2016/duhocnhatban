<!DOCTYPE html>
<html lang="en">
@include('template.header.head_cart')

<body>
@include('template.header.cart')
<section id="form" style="margin-top:0px"><!--form-->
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-1">
                <div class="login-form"><!--login form-->
                    <h2>Đăng nhập tài khoản</h2>
                    @include('admin.errors.error')
                    <div class="clearfix"></div>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <a class="btn btn-primary " style="background: #3B5998;border-radius: 5px;margin-bottom: 20px" href="{{ url('/auth/redirect/facebook') }}"><i class="fa fa-facebook"></i>  Login with Facebook</a>
                        </div>
                        <div class="col-md-6">
                            <a class="btn btn-primary " style="background: #cd1417;border-radius: 5px;margin-bottom: 20px" href="{{ url('/google') }}"><i class="fa fa-google"></i>  Login with Google</a>
                        </div>
                    </div>
                    <center><p>----- Hoặc đăng nhập bằng tài khoản -----</p></center>
                    <form action="{{asset('login')}}" method="post">
                        @csrf
                        <input type="email" placeholder="Email" name="email" />
                        <input type="password" name="pass" placeholder="Email Address" />
                        <span>
								<input type="checkbox" class="checkbox">
								Ghi nhớ tài khoản
							</span>
                        <button type="submit" class="btn btn-default">Đăng nhập</button>
                    </form>
                </div><!--/login form-->
            </div>
            <div class="col-sm-1">
                <h2 class="or">Hoặc</h2>
            </div>
            <div class="col-sm-4">
                <div class="signup-form"><!--sign up form-->
                    <h2>Đăng ký tài khoản!</h2>
                    @include('admin.errors.error')
                    <div class="clearfix"></div>
                    <br>
                    <br>
                    <form  method="post" action="{{asset('register')}}">
                        @csrf

                        <input type="text" placeholder="Họ và tên" name="name"/>
                        <input type="password" placeholder="Mật khẩu" name="pass"/>
                        <input type="password" placeholder="Nhập lại Mật khẩu" name="Re_pass"/>
                        <input type="number" placeholder="Số điện thoại" name="phone"/>
                        <input type="text" placeholder="Địa chỉ" name="address"/>
                        <p><input type="checkbox" name="type" value="1"  style="height: 20px;width: 30%"/> Mở shop trên website</p>
                        <button type="submit" class="btn btn-default">Đăng ký</button>
                    </form>
                </div><!--/sign up form-->
            </div>
        </div>
    </div>
</section><!--/form-->

@include('template.footer')

@extends('layout')
@section('content')
    <div class="category-tab shop-details-tab"><!--category-tab-->
                <h2 class="title text-center">Sản phẩm mới</h2>
                @if($product_new_cash!=null)

                    @foreach($product_new_cash as $p_n)

                        <div class="col-sm-12">
                            <div class="product-image-wrapper">
                                <div class="single-products">
                                    <div class="productinfo ">
                                        <a href="{{asset('detail/'.$p_n->product_name_slug)}}">
                                            <div class="col-md-3">
                                                <img src="{{asset('upload/product/'.$p_n->product_images)}}" alt="" width="100%">
                                            </div>

                                            <div class="col-md-9">

                                                @if($p_n->product_price_km!=0)
                                                    <h4 style="    text-decoration: line-through;color: #ccc">{{number_format($p_n->product_price,0,',','.')}} VNĐ</h4>
                                                    <h2 style="color:#FE980F;">{{number_format($p_n->product_price_km,0,',','.')}} VNĐ <sup style="color:red;font-size: 15px">-<?php
                                                            echo 100-ceil($p_n->product_price_km*100/$p_n->product_price).'%';
                                                            ?></sup></h2>
                                                @else
                                                    <h2 style="color:#FE980F;">{{number_format($p_n->product_price,0,',','.')}} VNĐ</h2>
                                                @endif
                                                <p>{{mysubstr($p_n->product_name,60)}}</p>
                                                <p>{{mysubstr($p_n->product_desc,100)}}</p>




                                            </div>
                                        </a>

                                    </div>
                                </div>
                    @endforeach
                @endif
                                @if($car!=null)


                                @foreach($car as $p_n)

                        <div class="col-sm-12">
                            <div class="product-image-wrapper">
                                <div class="single-products">
                                    <div class="productinfo ">
                                        <a href="{{asset('detail/'.$p_n->product_name_slug)}}">
                                            <div class="col-md-3">
                                                <img src="{{asset('upload/product/'.$p_n->product_images)}}" alt="" width="100%">
                                            </div>

                                            <div class="col-md-9">

                                                @if($p_n->product_price_km!=0)
                                                    <h4 style="    text-decoration: line-through;color: #ccc">{{number_format($p_n->product_price,0,',','.')}} VNĐ</h4>
                                                    <h2 style="color:#FE980F;">{{number_format($p_n->product_price_km,0,',','.')}} VNĐ <sup style="color:red;font-size: 15px">-<?php
                                                            echo 100-ceil($p_n->product_price_km*100/$p_n->product_price).'%';
                                                            ?></sup></h2>
                                                @else
                                                    <h2 style="color:#FE980F;">{{number_format($p_n->product_price,0,',','.')}} VNĐ</h2>
                                                @endif
                                                <p>{{mysubstr($p_n->product_name,60)}}</p>
                                                <p>{{mysubstr($p_n->product_desc,100)}}</p>




                                            </div>
                                        </a>

                                    </div>
                                </div>
                    @endforeach

                                @endif


        </div>
    </div><!--/category-tab-->





@endsection
<style>
    a.tags_style{
        margin: 3px 2px;
        border: 1px solid;
        height: auto;
        background: #428bca;
        color: #FFFFFF;padding: 0px;
        border-radius: 5px;
        padding: 3px;
    }
    a.tags_style:hover{
        background: black;
    }

    .category-tab ul {
        background: white!important;
    }
    .category-tab ul li a {
        font-size: 10px!important;

    }

    .features_items {
        overflow: visible!important;
    }
    .choose ul li a {
        color: #B3AFA8;
        font-family: 'Roboto', sans-serif;
        font-size: 13px;
        padding-left: 0;
        padding-right: 0;
    }
    .nav-tabs>li {
        float: right!important;
        margin-bottom: -1px;
    }

    h3.title-oo {
        background: #FE980F;
        color: white;
        padding: 12px;
    }
</style>

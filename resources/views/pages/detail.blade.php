@extends('layout')
@section('content')

    <nav aria-label="breadcrumb" >
        <ol class="breadcrumb" style="background: white">
            <li class="breadcrumb-item"><a href="{{asset('/')}}">Trang chủ </a></li>
            <li class="breadcrumb-item active" aria-current="page">{{$product->product_name}}</li>
        </ol>
    </nav>
    <div class="product-details"><!--product-details-->
        <div class="col-sm-5">
            <ul id="imageGallery">
                <li data-thumb="{{asset('upload/product/'.$product->product_images)}}" data-src="{{asset('upload/product/'.$product->product_images)}}">
                    <img width="100%" src="{{asset('upload/product/'.$product->product_images)}}" />
                </li>

            @if(count($product_img)>0)
                    @foreach($product_img as $p_i)
                <li data-thumb="{{asset('upload/product/gallyery/'.$p_i->images)}}" data-src="{{asset('upload/product/gallyery/'.$p_i->images)}}">
                    <img width="100%" src="{{asset('upload/product/gallyery/'.$p_i->images)}}" />
                </li>
                        @endforeach
                    @endif
            </ul>
        </div>
        <div class="col-sm-7">
            <div class="product-information"><!--/product-information-->
                <img src="{{asset('public/Eshopper/images/product-details/new.jpg')}}" class="newarrival" alt="" />
                <h2>{{$product->product_name}}</h2>

                <form method="post" action="{{asset('add-to-cart')}}">
                <span>
                                                @if($product->product_price_km!=0)

                    <span style="font-size: 16px;    text-decoration: line-through;color: #ccc">{{number_format($product->product_price,0,',','.')}} VNĐ</span>
                    <div class="clearfix"></div>
                        <span style="color:#FE980F;">{{number_format($product->product_price_km,0,',','.')}} VNĐ   <sup style="color:red;font-size: 15px">-<?php
                                echo 100-ceil($product->product_price_km*100/$product->product_price).'%';
                                ?></sup></span>
                    @else
                        <span style="color:#FE980F;">{{number_format($product->product_price,0,',','.')}} VNĐ</span>
                                                    @endif
                    <br>
                                                    <?php
                                                    $depot=DB::table('tbl_depot')->where('id_product',$product->id_product)->first();
                                                    ?>

                    <div class="clearfix"></div>

                </form>

            </div><!--/product-information-->
        </div>
    </div><!--/product-details-->

    <div class="category-tab shop-details-tab"><!--category-tab-->
        <div class="col-sm-12">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#details" data-toggle="tab">Mô tả</a></li>
                <li><a href="#companyprofile" data-toggle="tab">Chi tiết sản phẩm</a></li>

            </ul>
        </div>
        <div class="tab-content">
            <div class="tab-pane fade active in" id="details" >
                {!! $product->product_desc !!}
            </div>

            <div class="tab-pane fade" id="companyprofile" >
                {!! $product->product_content !!}
            </div>

            <div class="tab-pane fade" id="tag" >
                <div class="col-sm-3">
                    <div class="product-image-wrapper">
                        <div class="single-products">
                            <div class="productinfo text-center">
                                <img src="images/home/gallery1.jpg" alt="" />
                                <h2>$56</h2>
                                <p>Easy Polo Black Edition</p>
                                <button type="button" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="product-image-wrapper">
                        <div class="single-products">
                            <div class="productinfo text-center">
                                <img src="images/home/gallery2.jpg" alt="" />
                                <h2>$56</h2>
                                <p>Easy Polo Black Edition</p>
                                <button type="button" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="product-image-wrapper">
                        <div class="single-products">
                            <div class="productinfo text-center">
                                <img src="images/home/gallery3.jpg" alt="" />
                                <h2>$56</h2>
                                <p>Easy Polo Black Edition</p>
                                <button type="button" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="product-image-wrapper">
                        <div class="single-products">
                            <div class="productinfo text-center">
                                <img src="images/home/gallery4.jpg" alt="" />
                                <h2>$56</h2>
                                <p>Easy Polo Black Edition</p>
                                <button type="button" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade " id="reviews" >
                <div class="col-sm-12">
                    <p><b>Đánh giá sản phẩm </b></p>
                    <ul class="list-inline rating" title="">
                        @for($sao=1;$sao<=5;$sao++)
                            <?php
                            if($sao<=$rating){
                                $color="color:#ffcc00";
                            }else{
                                $color="color:#ccc";
                            }
                            ?>
                        <li title="Đánh giá sao"
                            id="{{$product->id_product}}-{{$sao}}"
                            data-index="{{$sao}}"
                            data-product_id="{{$product->id_product}}"
                            data-rating="{{$rating}}"

                            class="rating"
                            style="cursor: pointer;{{$color}};font-size: 30px">
                            &#9733
                        </li>
                        @endfor

                    </ul>
                </div>
            </div>

        </div>
    </div><!--/category-tab-->

    <div class="recommended_items"><!--recommended_items-->
        <h2 class="title text-center">Sản phẩm gợi ý</h2>

        <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="item active">
                    @foreach($product_goiy as $p)

                    <div class="col-sm-4">
                        <a href="{{asset('detail/'.$p->product_name_slug)}}">
                        <div class="product-image-wrapper">
                            <div class="single-products">
                                <div class="productinfo text-center">
                                    <img src="{{asset('upload/product/'.$p->product_images)}}" alt="" />
                                    <h2>{{number_format($p->product_price,0,',','.')}}</h2>
                                    <p>{{$p->product_name}}</p>

                                </div>
                            </div>
                        </div>
                        </a>
                    </div>
                    @endforeach
                </div>

            </div>
            <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                <i class="fa fa-angle-left"></i>
            </a>
            <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                <i class="fa fa-angle-right"></i>
            </a>
        </div>
    </div><!--/recommended_items-->



@endsection
<style>
    a.tags_style{
        margin: 3px 2px;
        border: 1px solid;
        height: auto;
        background: #428bca;
        color: #FFFFFF;padding: 0px;
        border-radius: 5px;
        padding: 3px;
    }
    a.tags_style:hover{
        background: black;
    }
</style>

@extends('layout')
@section('content')
     <div class="features_items"><!--features_items-->
        <h2 class="title text-center">Danh mục {{$category_id->category_name}}</h2>

        <div class="col-md-12">
            <div class="row">
                <div class="col-md-4">
                    <form>
                        <label >Sắp xếp theo</label>

                        <select id="sort" class="form-control">
                            <option value="{{Request::url()}}?sort_by=none">---Lọc---</option>
                            <option value="{{Request::url()}}?sort_by=tangdan">Giá Tăng dần</option>
                            <option value="{{Request::url()}}?sort_by=giamdan">Giá Giảm dần</option>
                            <option value="{{Request::url()}}?sort_by=kytu_az">Lọc theo tên từ A - Z</option>
                            <option value="{{Request::url()}}?sort_by=kytu_za">Lọc theo tên từ Z - A</option>
                        </select>
                    </form>
                </div>
                <div class="col-md-4">

                        <label >Lọc giá theo</label>
                    <form>
                        <div class="col-md-10">
                        <div class="min-max-slider" data-legendnum="2">
                            <label for="min">Minimum price</label>
                            <input id="min" class="min" name="min" type="range" step="1" min="0" max="{{$price_max->product_price}}" />
                            <label for="max">Maximum price</label>
                            <input id="max" class="max" name="max" type="range" step="1" min="0" max="{{$price_max->product_price}}" />
                        </div>
                        </div>
                        <div class="col-md-2">
                            <button class="btn-primary btn" >Lọc</button>

                        </div>

                    </form>

                </div>
            </div>
        </div>
         @if(count($category_product)>0)
        @foreach($category_product as $p_n)
            <?php

            $pro=DB::table('tbl_product_shop')->where('id_product',$p_n->id_product)->first();

            $pro_shop=DB::table('tbl_shop')->where('id_shop_user',$pro->id_shop)->first();

            ?>
        <div class="col-sm-4">
            <div class="product-image-wrapper">
                <div class="single-products">
                    <div class="productinfo text-center">
                        <a href="{{asset('detail/'.$p_n->product_name_slug)}}" ><img src="{{asset('upload/product/'.$p_n->product_images)}}" alt="" />
                          @if($p_n->product_price_km!=0)
                                <h4 style="    text-decoration: line-through;color: #ccc">{{number_format($p_n->product_price,0,',','.')}} VNĐ</h4>
                                <h2 style="color:#FE980F;">{{number_format($p_n->product_price_km,0,',','.')}} VNĐ <sup style="color:red;font-size: 15px">-<?php
                                        echo 100-ceil($p_n->product_price_km*100/$p_n->product_price).'%';
                                        ?></sup></h2>
                            @else
                                <h2 style="color:#FE980F;">{{number_format($p_n->product_price,0,',','.')}} VNĐ</h2>
                            @endif

                        <p>{{mysubstr($p_n->product_name,60)}}</p>
                        </a>
                        <div class="shop_info"><a href="{{asset('shop/'.$pro_shop->id_shop_user)}}"> <i class="fa fa-bank"></i> <b>{{$pro_shop->name_shop}}</b></a></div>
                        <a href="{{asset('add-to-cart/'.$p_n->id_product)}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Thêm vào giỏ hàng</a>
                    </div>
                </div>
            </div>

            <div class="choose">
                <ul class="nav nav-pills nav-justified">
                    <li><a href="{{asset('wishlist/'.$p_n->id_product)}}"><i class="fa fa-plus-square"></i>Thêm vào yêu thích</a></li>
                    <li><a href="{{asset('compare/'.$p_n->id_product)}}"><i class="fa fa-plus-square"></i>So sánh</a></li>
                </ul>
            </div>
        </div>
        @endforeach
         @else
         <p>Không có sản phẩm phù hợp</p>
         @endif
    </div><!--features_items-->



<style>

    .min-max-slider {position: relative; width: 200px; text-align: center; margin-bottom: 50px;}
    .min-max-slider > label {display: none;}
    span.value {height: 1.7em; font-weight: bold; display: inline-block;}
    span.value.lower::before {content: ""; display: inline-block;}
    span.value.upper::before {content: "- "; display: inline-block; margin-left: 0.4em;}
    .min-max-slider > .legend {display: flex; justify-content: space-between;}
    .min-max-slider > .legend > * {font-size: small; opacity: 0.25;}
    .min-max-slider > input {cursor: pointer; position: absolute;}

    /* webkit specific styling */
    .min-max-slider > input {
        -webkit-appearance: none;
        outline: none!important;
        background: transparent;
        background-image: linear-gradient(to bottom, transparent 0%, transparent 30%, silver 30%, silver 60%, transparent 60%, transparent 100%);
    }
    .min-max-slider > input::-webkit-slider-thumb {
        -webkit-appearance: none; /* Override default look */
        appearance: none;
        width: 14px; /* Set a specific slider handle width */
        height: 14px; /* Slider handle height */
        background: #eee; /* Green background */
        cursor: pointer; /* Cursor on hover */
        border: 1px solid gray;
        border-radius: 100%;
    }
    .min-max-slider > input::-webkit-slider-runnable-track {cursor: pointer;}
</style>
<script>
    var thumbsize = 14;

    function draw(slider,splitvalue) {

        /* set function vars */
        var min = slider.querySelector('.min');
        var max = slider.querySelector('.max');
        var lower = slider.querySelector('.lower');
        var upper = slider.querySelector('.upper');
        var legend = slider.querySelector('.legend');
        var thumbsize = parseInt(slider.getAttribute('data-thumbsize'));
        var rangewidth = parseInt(slider.getAttribute('data-rangewidth'));
        var rangemin = parseInt(slider.getAttribute('data-rangemin'));
        var rangemax = parseInt(slider.getAttribute('data-rangemax'));

        /* set min and max attributes */
        min.setAttribute('max',splitvalue);
        max.setAttribute('min',splitvalue);

        /* set css */
        min.style.width = parseInt(thumbsize + ((splitvalue - rangemin)/(rangemax - rangemin))*(rangewidth - (2*thumbsize)))+'px';
        max.style.width = parseInt(thumbsize + ((rangemax - splitvalue)/(rangemax - rangemin))*(rangewidth - (2*thumbsize)))+'px';
        min.style.left = '0px';
        max.style.left = parseInt(min.style.width)+'px';
        min.style.top = lower.offsetHeight+'px';
        max.style.top = lower.offsetHeight+'px';
        legend.style.marginTop = min.offsetHeight+'px';
        slider.style.height = (lower.offsetHeight + min.offsetHeight + legend.offsetHeight)+'px';

        /* correct for 1 off at the end */
        if(max.value>(rangemax - 1)) max.setAttribute('data-value',rangemax);

        /* write value and labels */
        max.value = max.getAttribute('data-value');
        min.value = min.getAttribute('data-value');
        lower.innerHTML = min.getAttribute('data-value');
        upper.innerHTML = max.getAttribute('data-value');

    }

    function init(slider) {
        /* set function vars */
        var min = slider.querySelector('.min');
        var max = slider.querySelector('.max');
        var rangemin = parseInt(min.getAttribute('min'));
        var rangemax = parseInt(max.getAttribute('max'));
        var avgvalue = (rangemin + rangemax)/2;
        var legendnum = slider.getAttribute('data-legendnum');

        /* set data-values */
        min.setAttribute('data-value',rangemin);
        max.setAttribute('data-value',rangemax);

        /* set data vars */
        slider.setAttribute('data-rangemin',rangemin);
        slider.setAttribute('data-rangemax',rangemax);
        slider.setAttribute('data-thumbsize',thumbsize);
        slider.setAttribute('data-rangewidth',slider.offsetWidth);

        /* write labels */
        var lower = document.createElement('span');
        var upper = document.createElement('span');
        lower.classList.add('lower','value');
        upper.classList.add('upper','value');
        lower.appendChild(document.createTextNode(rangemin));
        upper.appendChild(document.createTextNode(rangemax));
        slider.insertBefore(lower,min.previousElementSibling);
        slider.insertBefore(upper,min.previousElementSibling);

        /* write legend */
        var legend = document.createElement('div');
        legend.classList.add('legend');
        var legendvalues = [];
        for (var i = 0; i < legendnum; i++) {
            legendvalues[i] = document.createElement('div');
            var val = Math.round(rangemin+(i/(legendnum-1))*(rangemax - rangemin));
            legendvalues[i].appendChild(document.createTextNode(val));
            legend.appendChild(legendvalues[i]);

        }
        slider.appendChild(legend);

        /* draw */
        draw(slider,avgvalue);

        /* events */
        min.addEventListener("input", function() {update(min);});
        max.addEventListener("input", function() {update(max);});
    }

    function update(el){
        /* set function vars */
        var slider = el.parentElement;
        var min = slider.querySelector('#min');
        var max = slider.querySelector('#max');
        var minvalue = Math.floor(min.value);
        var maxvalue = Math.floor(max.value);

        /* set inactive values before draw */
        min.setAttribute('data-value',minvalue);
        max.setAttribute('data-value',maxvalue);

        var avgvalue = (minvalue + maxvalue)/2;

        /* draw */
        draw(slider,avgvalue);
    }

    var sliders = document.querySelectorAll('.min-max-slider');
    sliders.forEach( function(slider) {
        init(slider);
    });
</script>
@endsection


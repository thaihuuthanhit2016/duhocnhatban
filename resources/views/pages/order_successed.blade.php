<!DOCTYPE html>
<html lang="en">
@include('template.header.head_cart')

<body>
@include('template.header.cart')
<section id="cart_items">
    <div class="container">

            <h4>Cám ơn bạn đã thanh toán thành công  ! Chúng tôi đang xử lý đơn hàng của bạn !</h4>
            <h5>Mã đơn hàng: <b style="color:#FE980F">{{$bill->code_bill}}</b></h5>
            <a class="btn btn-primary" href="{{asset('/')}}">Tiếp tục mua sắm</a>
        <div class="clearfix"></div>
        <br>
    </div>
</section> <!--/#cart_items-->

@include('template.footer')

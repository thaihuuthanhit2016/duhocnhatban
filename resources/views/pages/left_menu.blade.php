<div class="left-sidebar">
    <h2>Quản lý giao dịch</h2>
    <div class="panel-group category-products" id="accordian"><!--category-productsr-->

        <div class="panel panel-default">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title" style="margin-bottom: 10px"><a id="order" href="{{asset('order/')}}"><i class="fa fa-list-alt" style="color:#FE980F;"></i> &nbsp;&nbsp;Đơn hàng</a></h4>
                    <h4 class="panel-title" style="margin-bottom: 10px"><a id="wallet" href="{{asset('wallet/')}}"><i class="fa fa-user"  style="color:#FE980F;"></i>&nbsp;&nbsp;&nbsp;Ví BitConSJC</a></h4>
                    <h4 class="panel-title" style="margin-bottom: 10px"><a  id="wishlist" href="{{asset('wishlist/')}}"><i class="fa fa-heart"  style="color:#FE980F;"></i>&nbsp;&nbsp;Sản Phẩm yêu thích</a></h4>
                </div>
            </div>


        </div>

    </div>
    <h2>Quản lý tài khoản</h2>
    <div class="panel-group category-products" id="accordian"><!--category-productsr-->

        <div class="panel panel-default">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title " style="margin-bottom: 10px"><a id="account" href="{{asset('account/')}}" ><i class="fa fa-edit" style="color:#FE980F;"></i> &nbsp;&nbsp;Thông tin tài khoản</a></h4>
                    <h4 class="panel-title" style="margin-bottom: 10px"><a id="offer" href="{{asset('offer')}}"><i class="fa fa-user"  style="color:#FE980F;"></i>&nbsp;&nbsp;&nbsp;Ưu đãi của tôi</a></h4>
                    <h4 class="panel-title" style="margin-bottom: 10px"><a  id="multiple_choice" href="{{asset('multiple_choice')}}"><i class="fa fa-user"  style="color:#FE980F;"></i>&nbsp;&nbsp;&nbsp;Lấy mã voucher</a></h4>
                    <h4 class="panel-title" style="margin-bottom: 10px"><a id="viewed_producut" href="{{asset('viewed_producut')}}"><i class="fa fa-user"  style="color:#FE980F;"></i>&nbsp;&nbsp;&nbsp;Sản phẩm đã xem</a></h4>

                </div>
            </div>


        </div>

    </div><!--/category-products-->
</div>

@extends('layout')
@section('title','Xe')

@section('content')
    <div class="features_items"><!--features_items-->
        <h2 class="title text-center">Sản phẩm </h2>
        @include('admin.errors.error')
        <br>
        <div class="clearfix"></div>
        <br>
        <?php
        $i=0;
        ?>
        <div class="row">
        @foreach($car as $p_n)
            <?php
            $i++;
            $category_all=DB::table('tbl_category_product')->where('id_category_product',$p_n->category_id)->first();


            ?>
            <?php


            ?>
            <div class="col-sm-4">
                <div class="product-image-wrapper">
                    <div class="single-products">
                        <div class="productinfo text-center">

                            <a href="{{asset('detailcar/'.$p_n->product_name_slug)}}" ><img src="{{asset('upload/product/'.$p_n->product_images)}}" alt="" />
                                @if($p_n->product_price_km!=0)
                                    <h4 style="    text-decoration: line-through;color: #ccc">{{number_format($p_n->product_price,0,',','.')}} VNĐ</h4>
                                    <h2 style="color:#FE980F;">{{number_format($p_n->product_price_km,0,',','.')}} VNĐ</h2>
                                @else
                                    <h2 style="color:#FE980F;">{{number_format($p_n->product_price,0,',','.')}} VNĐ</h2>
                                @endif
                            </a>
                            <p>{{$p_n->product_name}}</p>
                            </a>
                            {{--                        <a style="cursor: pointer" class="btn btn-default xemnhanh" data-product_id="{{$p_n->id_product}}" data-toggle="modal" data-target="#xemnhanh_{{$p_n->product_name_slug}}_{{$i}}"><i class="fa fa-eye"></i>Xem nhanh</a>--}}
                        </div>
                    </div>
                </div>

                <div class="choose">
                    <ul class="nav nav-pills nav-justified">
                        <li><a href="{{asset('wishlist/'.$p_n->id_product)}}"><i class="fa fa-plus-square"></i>Thêm vào yêu thích</a></li>
                        <li><a href="{{asset('compare/'.$p_n->id_product)}}"><i class="fa fa-plus-square"></i>So sánh</a></li>
                    </ul>
                </div>
            </div>








        @endforeach
        </div>
    </div><!--features_items-->



@endsection
<style>
    .title-oo{
        padding: 10px;
        color: white;
        background: #FE980F;
    }
</style>

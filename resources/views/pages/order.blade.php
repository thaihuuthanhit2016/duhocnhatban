<!DOCTYPE html>
<html lang="en">
@include('template.header.head_cart')

<body>
@include('template.header.cart')
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                @include('pages.left_menu')
            </div>

            <div class="col-sm-9">
                    <h3 class="title">DANH SÁCH ĐƠN HÀNG ĐÃ MUA</h3>
                <br>
                <div class="table-responsive cart_info">
                    <table class="table table-condensed" border="1">
                        <thead>
                        <?php
                        $addressvi="lGAJDaThG28Y7UtGTU7jJ1apRLczA2PfdUUSw1";
                        $i=0;
                        ?>
                        @foreach($bill as $b)
                            <?Php
                                $i++;
                                $detail_bill=DB::table('tbl_bill_detail')->where('id_bill',$b->id_bill)->get();
                            ?>
                            @foreach($detail_bill as $d_b)

                                <?Php
                                $product=DB::table('tbl_product')->where('id_product',$d_b->id_product)->first();
                                $bill_payment=DB::table('tbl_payment')->where('id_bill',$b->id_bill)->first();
                                ?>
                                @if($bill_payment->payment_method==3)

                                    <tr class="cart_menu">
                            <td class="image" style="width: 100px;">Mã đơn hàng:<br><b> {{$b->code_bill}}</b></td>

                            <td class="price" style="width: 120px">Tên sản phẩm: <br> <b>{{$product->product_name}}</b></td>
                            <td class="quantity" style="width: 40px">Giá:<br><b> @if($product->product_price_km!=0){{number_format($product->product_price_km*800/20000,0,',','.')}} @else {{number_format($product->product_price*800/20000,0,',','.')}} @endif BITCOINSJC</b></td>
                            <td class="total"style="width: 70px">Số lượng: <br><b>{{$d_b->quantity}}</b></td>
                            <td class="total"style="width: 80px">Tổng tiền: <br><b>@if($product->product_price_km!=0){{number_format($b->total,0,',','.')}} @else {{number_format($b->total,0,',','.')}} @endif BITCOINSJC</b></td>
                            <td class="total"style="width: 120px">Trạng thái:<br> <b>@if($b->status==0)<a href="http://localhost/coindemo_1/payment/{{$b->total}}/{{$b->code_bill}}/{{$addressvi}}" style="color: #fe980F"> Chưa Thanh toán</a> @else Đã Thanh toán @endif</b></td>
                                        <td class="total" style="width: 80px;"> @if($b->status==1)<a style="color:#FE980F;" href="{{asset('add-to-cart/'.$product->id_product)}}">Mua lại</a> @endif <br> @if($d_b->rate==0)<a style="color:#FE980F;cursor: pointer" data-toggle="modal" data-target="#myModal_{{$i}}" >Đánh giá</a> @else Đã đánh giá @endif</td>


                        </tr>

                                @endif

                                @if($bill_payment->payment_method!=3)

                                    <tr class="cart_menu">
                                        <td class="image" style="width: 100px;">Mã đơn hàng:<br><b> {{$b->code_bill}}</b></td>

                                        <td class="price" style="width: 120px">Tên sản phẩm: <br> <b>{{$product->product_name}}</b></td>
                                        <td class="quantity" style="width: 40px">Giá:<br><b> @if($product->product_price_km!=0){{number_format($product->product_price_km,0,',','.')}} @else {{number_format($product->product_price,0,',','.')}} @endif VNĐ</b></td>
                                        <td class="total"style="width: 70px">Số lượng: <br><b>{{$d_b->quantity}}</b></td>
                                        <td class="total"style="width: 80px">Tổng tiền: <br><b>@if($product->product_price_km!=0){{number_format($b->total,0,',','.')}} @else {{number_format($b->total,0,',','.')}} @endif VNĐ</b></td>
                                        <td class="total"style="width: 120px">Trạng thái:<br> <b>@if($b->status==0) Chưa Thanh toán @else Đã Thanh toán @endif</b></td>
                                        <td class="total" style="width: 80px;"> <a style="color:#FE980F;" href="{{asset('add-to-cart/'.$product->id_product)}}">Mua lại</a></td>


                                    </tr>

                                @endif
                            @endforeach
                        @endforeach
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</section>

@include('template.footer')
<style>
    .active{
        color: #FE980F!important;
    }
</style>

<script>
    $(document).ready(function (){
        $('#order').addClass('active');
    });
</script>
<?php
$i=0;
?>
@foreach($bill as $b)
    <?Php
        $i++;
    $detail_bill=DB::table('tbl_bill_detail')->where('id_bill',$b->id_bill)->get();
    ?>
    @foreach($detail_bill as $d_b)

        <?Php
        $product=DB::table('tbl_product')->where('id_product',$d_b->id_product)->first();
        $bill_payment=DB::table('tbl_payment')->where('id_bill',$b->id_bill)->first();
        ?>
        @if($bill_payment->payment_method==3)

<div id="myModal_{{$i}}" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <ul class="list-inline rating" title="">
                    {{$product->product_name}}
                    @for($sao=1;$sao<=5;$sao++)

                        <li title="Đánh giá sao"
                            id="{{$product->id_product}}-{{$sao}}"
                            data-index="{{$sao}}"
                            data-product_id="{{$product->id_product}}"

                            class="rating"
                            style="cursor: pointer;color: #ccc;font-size: 30px">
                            &#9733
                        </li>
                    @endfor

                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
@endif
@endforeach
@endforeach
<script>
    function  remove_background(product_id){
        for(var count=1;count<=5;count++){
            $("#"+product_id+'-'+count).css('color','#ccc');
        }
    }

    $(document).on('mouseenter','.rating',function (){
        var index =$(this).data('index');
        var product_id=$(this).data('product_id');

        remove_background(product_id);
        for(var count=1;count<=index;count++){
            $("#"+product_id+'-'+count).css('color','#ffcc00');
        }
    });
    $(document).on('mouseleave','.rating',function (){
        var index =$(this).data('index');
        var product_id=$(this).data('product_id');
        var rating=$(this).data('rating');
        remove_background(product_id);
        for(var count=1;count<=rating;count++){
            $("#"+product_id+'-'+count).css('color','#ffcc00');
        }
    });
    $(document).on('click','.rating',function (){
        var index =$(this).data('index');
        var product_id=$(this).data('product_id');
        $.ajax({
            url:"{{asset('rating')}}",
            method:'get',
            data:{index:index,product_id:product_id},
            success:function (data){
                if(data=="done"){
                    location.reload();
                }else {
                    alert("Lỗi đánh giá");
                }
            }
        })
    });
</script>

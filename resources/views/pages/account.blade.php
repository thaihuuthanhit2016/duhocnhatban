<!DOCTYPE html>
<html lang="en">
@include('template.header.head_cart')

<body>
@include('template.header.cart')
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                @include('pages.left_menu')
            </div>

            <div class="col-sm-6">
                    <h3 class="title">THÔNG TIN TÀI KHOẢN</h3>
                <br>
                @if($account->verifyKYC=='N')
                    <p class="alert-danger alert"> Vui lòng cập nhật KYC</p>
                @endif
                @if($account->verifyKYC=='P')
                    <p class="alert-warning alert"> KYC của bạn đang chờ duyệt</p>
                @endif
                @if($account->verifyKYC=='Y')
                    <p class="alert-success alert"> KYC của bạn đã duyệt</p>
                @endif
                <div class="clearfix"></div>
                @include('admin.errors.error')
                <div class="clearfix"></div>
                <br>
                <form method="post" action="{{asset('update_profile')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <input readonly class="form-control" value="{{$account->customer_email}}" >
                        <input type="hidden" class="form-control"name="id_customer" value="{{$account->id_customer}}" >
                    </div>
                    <div class="form-group">
                        <input type="number" class="form-control" name="phone" value="{{$account->customer_phone}}" >
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="name" value="{{$account->customer_name}}" >
                    </div>
                    <div class="form-group">
                        <label>Mặt trước CCCD/CMND/HC</label>
                        <input type="file"  name="images1"  >
                        @if($account->images1==null)
                            <div class="clearfix"></div>
                            <br>
                            <img src="{{asset('upload/shop/cmnd_mt.jpg')}}" width="100px">

                        @else
                            <div class="clearfix"></div>
                            <br>
                            <img src="{{asset('upload/customer/'.$account->images1)}}" width="100px">
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Mặt sau CCCD/CMND/HC</label>
                        <input type="file"  name="images2"  >
                        @if($account->images2==null)
                            <div class="clearfix"></div>

                            <br>
                        <img src="{{asset('upload/shop/cmnd_ms.jpg')}}" width="100px">

                        @else
                            <div class="clearfix"></div>

                            <br>
                        <img src="{{asset('upload/customer/'.$account->images2)}}" width="100px">
                        @endif
                    </div>
                    <div class="form-group">
                        <label><a href="{{asset('change-password')}}">Thay đổi mật khẩu</a> </label>
                    </div>
@if($account->verifyKYC=='N')
                <div class="form-group">
                    <input type="submit" class="btn-primary btn" value="Cập nhật" >
                </div>
                    @endif
            </form>

            </div>
        </div>
    </div>
</section>

@include('template.footer')
<style>
    .active{
        color: #FE980F!important;
    }
</style>
<script>
    $(document).ready(function (){
        $('#account').addClass('active');
    });
</script>

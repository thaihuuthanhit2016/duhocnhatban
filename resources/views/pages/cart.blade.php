<!DOCTYPE html>
<html lang="en">
@include('template.header.head_cart')

<body>
@include('template.header.cart')
<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="{{asset('/')}}">Trang chủ</a></li>
                <li class="active">Giỏ hàng</li>
            </ol>
        </div>
        <div class="table-responsive cart_info">
            <table class="table table-condensed">
                <thead>
                <tr class="cart_menu">
                    <td class="image">Sản phẩm</td>
                    <td class="description"></td>
                    <td class="price">Giá</td>
                    <td class="quantity">Số lượng</td>
                    <td class="total">Tổng tiền</td>
                    <td></td>
                </tr>
                </thead>
                <tbody>
                <?php
                $i=0;
                ?>
                @foreach($content as $c)
                    <?php
                        $i++;
                    $check_product=DB::table('tbl_product')->where('id_product',$c->id)->first();
                    $depot=DB::table('tbl_depot')->where('id_product',$c->id)->first();

                    ?>
                    <tr>
                        <td class="cart_product">
                            <a href="{{asset('detail/'.$check_product->product_name_slug)}}"><img src="{{asset('upload/product/'.$c->options->image)}}" width="70px" alt=""></a>
                        </td>
                        <td class="cart_description">
                            <h4><a href="">{{$c->name}}</a></h4>
                            <p>Mã sản phẩm: {{$check_product->code_product}}</p>
                        </td>
                        <td class="cart_price">
                            <p>{{number_format($c->price,0,',','.')}} VNĐ</p>
                        </td>
                        <td class="cart_quantity">
                            <form method="post" action="{{asset('updatecart')}}">
                                @csrf
                                <div class="cart_quantity_button">

                                    <input  class="cart_quantity_input qty_{{$i}}" type="number" style="
    height: 32px;" name="quantity" value="{{$c->qty}}" autocomplete="off" min="1" size="2">&nbsp;&nbsp;
                                    <input  class="cart_quantity_input id_product_{{$i}}" type="hidden" name="id" value="{{$c->rowId}}"  min="1" size="2">
                                    <input  class="btn-primary btn"style="margin-top: 0px" type="submit"  value="Update">

                                </div>
                            </form>
                        </td>
                        <td class="cart_total">
                            <p class="cart_total_price result_{{$i}}">@if($check_product->product_price_km!=0){{number_format($check_product->product_price_km*$c->qty,0,',','.')}}@else
                                    {{number_format($check_product->product_price*$c->qty,0,',','.')}} @endif VNĐ
                            </p>
                        </td>
                        <td class="cart_delete">
                            <a class="cart_quantity_delete" href="{{asset('delCart/'.$c->rowId)}}"><i class="fa fa-times"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>

</section> <!--/#cart_items-->

<section id="do_action">
    <div class="container">
        <div class="row">

            <div class="col-sm-6">
                <div class="total_area">
                    <ul>
                        <li>Tạm tính <span id="subtotal">{{Cart::subtotal()}} VNĐ</span></li>


                        <li>Thành tiền <span id="total">

                                    {{Cart::subtotal()}} VNĐ</span></li>

                    </ul>
                    <a class="btn btn-default check_out" href="{{asset('payment')}}">Thanh toán </a>
                </div>
            </div>
        </div>
    </div>
</section><!--/#do_action-->

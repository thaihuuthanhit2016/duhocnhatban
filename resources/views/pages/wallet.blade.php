<!DOCTYPE html>
<html lang="en">
@include('template.header.head_cart')

<body>
@include('template.header.cart')
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
@include('pages.left_menu')            </div>

            <div class="col-sm-6">
                    <h3 class="title">THÔNG TIN WALLET BITCOINSJC</h3>
                <br>
                <div class="clearfix"></div>
                <h4>Tỉ giá <b style="color:blue;">USD</b>-><span style="color:#FE980F">BITCOINSJC</span>: 1 USD= <span style="color:#FE980F">800 COIN</span></h4>
                <h4>Tỉ giá <b style="color:blue;">USD</b>-><span style="color:#FE980F">VNĐ</span>: 1 USD= <span style="color:#FE980F">23.000 VNĐ</span></h4>
                <br>

                <div class="clearfix"></div>
                @include('admin.errors.error')
                <div class="clearfix"></div>
                <br>
                <form method="post" action="{{asset('update_wallet')}}">
                    @csrf
                    <div class="form-group">
                        <input readonly class="form-control" value="{{$account->customer_email}}" >
                        <input type="hidden" class="form-control"name="id_customer" value="{{$account->id_customer}}" >
                    </div>
                    <div class="form-group">

                        <input type="text" @if($account->wallet_address!=null) readonly @endif class="form-control"  value="{{$wallet->wallet_address}}" id="address_wallet" name="address_wallet" placeholder="Your BitcoinSJC wallet address ">

                    </div>
                    <div class="form-group">

                        <input type="number"  class="form-control"  value="{{$wallet->wallet_address}}" id="coin_deposit" name="coin_deposit" placeholder="Amount of coins to deposit
">
                        <p id="kq" style="color:red;"></p>
                        <p id="kq2" style="color:blue;"></p>

                        <a style="cursor: pointer" id="check">Kiểm tra </a>
                    </div>
                    <div class="form-group">

                        <input type="text" readonly id="coin" class="form-control"  value="{{number_format($wallet->wallet_address_bitcoinsjc,0,',','.')}} BITCOINSJC" name="coin">
                    </div>

                <div class="form-group">
                    <input type="submit" id="lll" disabled="true" class="btn-primary btn" value="Cập nhật" >
                </div>
            </form>

            </div>
        </div>
    </div>
</section>

@include('template.footer')
<style>
    .active{
        color: #FE980F!important;
    }
</style>
<script>
    $('#check').click(function (){
var  address_wallet=$("#address_wallet").val();
var  coin_deposit=$("#coin_deposit").val();
if(address_wallet==''){
    var error='<div class="alert alert-danger">You must enter the address BitcoinSJC</div>'
    $('#kq').html(error);
return false;
}
if(coin_deposit==''){
    var error='<div class="alert alert-danger">You must enter the number of coins to deposit</div>'
    $('#kq').html(error);
return false;
}
        $.ajax({
            url : "http://localhost/shopbitcoin/apigetallcoin.php?address="+address_wallet+"&coin="+coin_deposit, // gửi ajax đến file result.php
            type : "get", // chọn phương thức gửi là get
            dateType:"text", // dữ liệu trả về dạng text

            success : function (result){
                // Sau khi gửi và kết quả trả về thành công thì gán nội dung trả về
                // đó vào thẻ div có id = result
                var tach=result.split('wallet:');
                var tach1=tach[0].split('coin: ');

                var tach2=tach1[1].split('<br />');

                if(tach2[0]!='false'){
                    $('#kq').html('Đã xác thực ví của bạn');
                    var tach=result.split('wallet');
                        var tach1=tach[0].split('coin: ');
                        var tach2=tach1[1].split('<br />');
                        if(coin_deposit>tach2[0]){

                            var error='<div class="alert alert-danger">The number of coins you deposit is larger than the balance you have </div>'
                            $('#kq').html(error);
                            return false;

                        }
                    $('#lll').prop('disabled', false);



                    $('#coin').val(tach2[0]+" BITCOINSJC");

                }else{
                    var error='<div class="alert alert-danger">Your wallet address does not exist </div>'

                    $('#kq').html(error);
                }
            }
        });
    });

</script>

<script>
    $(document).ready(function (){
        $('#wallet').addClass('active');
    });
</script>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php
        $config=DB::table('tbl_config')->first();

    ?>
    <title>{{$config->title_website}} | {{$pages->title}}</title>
    <link href="{{asset('public/Eshopper/css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link href="{{asset('public/Eshopper/css/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{asset('public/Eshopper/css/price-range.css')}}" rel="stylesheet">
    <link href="{{asset('public/Eshopper/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('public/Eshopper/css/main.css')}}" rel="stylesheet">
    <link href="{{asset('public/Eshopper/css/responsive.css')}}" rel="stylesheet">
<!--[if lt IE 9]>
    <script src="{{asset('public/Eshopper/js/html5shiv.js')}}"></script>
    <script src="{{asset('public/Eshopper/js/respond.min.js')}}"></script>
    <![endif]-->
    <link rel="shortcut icon" href="{{asset('public/Eshopper/images/ico/favicon.ico')}}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset('public/Eshopper/images/ico/apple-touch-icon-144-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('public/Eshopper/images/ico/apple-touch-icon-114-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('public/Eshopper/images/ico/apple-touch-icon-72-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" href="{{asset('public/Eshopper/images/ico/apple-touch-icon-57-precomposed.png')}}">
</head><!--/head-->

<body>
<header id="header"><!--header-->
    <div class="header_top"><!--header_top-->
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="contactinfo">
                        <ul class="nav nav-pills">
                            <li><a href="#"><i class="fa fa-phone"></i> +2 95 01 88 821</a></li>
                            <li><a href="#"><i class="fa fa-envelope"></i> info@domain.com</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="social-icons pull-right">
                        <ul class="nav navbar-nav">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header_top-->

    <div class="header-middle"><!--header-middle-->
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="logo pull-left">
                        <a href="{{asset('/')}}"><img src="{{asset('upload/'.$config->logo_header)}}" alt="" /></a>
                    </div>

                </div>
                <div class="col-sm-8">
                </div>
            </div>
        </div>
    </div><!--/header-middle-->

    <div class="header-bottom"><!--header-bottom-->
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="mainmenu pull-left">
                        <ul class="nav navbar-nav collapse navbar-collapse">
                            <li><a href="{{asset('/')}}" class="active">Trang chủ</a></li>
                            <?Php
                            $gioithieu=DB::table('tbl_pages')->where('type',0)->first();
                            ?>
                            @if($gioithieu!=null)
                                <li class="dropdown"><a href="{{asset('introduce')}}">{{$gioithieu->title}}</a>

                                </li>
                            @endif
                            <li class="dropdown"><a href="{{asset('product')}}">Du học</a>

                            </li>
                            <li class="dropdown"><a href="{{asset('car')}}">Xe</a>

                            </li>
                            <li><a href="{{asset('contact')}}">Liên hệ</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="search_box pull-right">
                        <form method="post" action="{{asset('search')}}">
                            @csrf
                            <input  type="text" name="keyword" placeholder="Tìm kiếm sản phẩm"/>
                            <input  style="margin-top: 0px;color:white;"type="submit" class="btn btn-primary" value="Tìm"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header-bottom-->
</header><!--/header-->

<section>
    <div class="container">
        <div class="row">

            <div class="col-sm-12 ">
                <div class="features_items"><!--features_items-->
                    <h2 class="title text-center">{{$pages->title}}</h2>
                    @include('admin.errors.error')
                    <br>
                    <div class="clearfix"></div>
                    <br>
                    <div class="content">
                        {!! $pages->content !!}
                    </div>


                    <div class="clearfix"></div>
                    <br>
                </div><!--features_items-->
            </div>
        </div>
    </div>
</section>

<footer id="footer"><!--Footer-->
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-2">
                    <div class="companyinfo">
                        <h2><span>e</span>-shopper</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,sed do eiusmod tempor</p>
                    </div>
                </div>
                <div class="col-sm-7">
                    <div class="col-sm-3">
                        <div class="video-gallery text-center">
                            <a href="#">
                                <div class="iframe-img">
                                    <img src="{{asset('public/Eshopper/images/home/iframe1.png')}}" alt="" />
                                </div>
                                <div class="overlay-icon">
                                    <i class="fa fa-play-circle-o"></i>
                                </div>
                            </a>
                            <p>Circle of Hands</p>
                            <h2>24 DEC 2014</h2>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="video-gallery text-center">
                            <a href="#">
                                <div class="iframe-img">
                                    <img src="{{asset('public/Eshopper/images/home/iframe2.png')}}" alt="" />
                                </div>
                                <div class="overlay-icon">
                                    <i class="fa fa-play-circle-o"></i>
                                </div>
                            </a>
                            <p>Circle of Hands</p>
                            <h2>24 DEC 2014</h2>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="video-gallery text-center">
                            <a href="#">
                                <div class="iframe-img">
                                    <img src="{{asset('public/Eshopper/images/home/iframe3.png')}}" alt="" />
                                </div>
                                <div class="overlay-icon">
                                    <i class="fa fa-play-circle-o"></i>
                                </div>
                            </a>
                            <p>Circle of Hands</p>
                            <h2>24 DEC 2014</h2>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="video-gallery text-center">
                            <a href="#">
                                <div class="iframe-img">
                                    <img src="{{asset('public/Eshopper/images/home/iframe4.png')}}" alt="" />
                                </div>
                                <div class="overlay-icon">
                                    <i class="fa fa-play-circle-o"></i>
                                </div>
                            </a>
                            <p>Circle of Hands</p>
                            <h2>24 DEC 2014</h2>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="address">
                        <img src="{{asset('public/Eshopper/images/home/map.png')}}" alt="" />
                        <p>505 S Atlantic Ave Virginia Beach, VA(Virginia)</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-widget">
        <div class="container">
            <div class="row">
                <div class="col-sm-2">
                    <div class="single-widget">
                        <h2>Chính sách</h2>
                        <ul class="nav nav-pills nav-stacked">
                            <?php
                            $chinhsach=DB::table('tbl_pages')->where('type',0)->get();
                            $huongdan=DB::table('tbl_pages')->where('type',1)->get();
                            $khuyenmai=DB::table('tbl_pages')->where('type',2)->get();
                            $dichvu=DB::table('tbl_pages')->where('type',3)->get();
                            ?>
                            @foreach($chinhsach as $c)
                                <li><a href="{{asset('pages/'.$c->title_slug)}}">{{$c->title}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="single-widget">
                        <h2>Hướng dẫn</h2>
                        <ul class="nav nav-pills nav-stacked">
                            @foreach($huongdan as $c)
                                <li><a href="{{asset('pages/'.$c->title_slug)}}">{{$c->title}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="single-widget">
                        <h2>Khuyến mãi</h2>
                        <ul class="nav nav-pills nav-stacked">

                            @foreach($khuyenmai as $c)
                                <li><a href="{{asset('pages/'.$c->title_slug)}}">{{$c->title}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="single-widget">
                        <h2>Dịch vụ</h2>
                        <ul class="nav nav-pills nav-stacked">

                            @foreach($dichvu as $c)
                                <li><a href="{{asset('pages/'.$c->title_slug)}}">{{$c->title}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3 col-sm-offset-1">
                    <div class="single-widget">
                        <h2>About Shopper</h2>
                        <form action="#" class="searchform">
                            <input type="text" placeholder="Your email address" />
                            <button type="submit" class="btn btn-default"><i class="fa fa-arrow-circle-o-right"></i></button>
                            <p>Get the most recent updates from <br />our site and be updated your self...</p>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <p class="pull-left">Copyright © 2018 BitCoinSJC Inc. All rights reserved.</p>
                <p class="pull-right">Designed by BitCoinSJC and Sponsored by BitCoinSJC</p>
            </div>
        </div>
    </div>

</footer><!--/Footer-->


<script src="{{asset('public/Eshopper/js/jquery.js')}}"></script>
<script src="{{asset('public/Eshopper/js/bootstrap.min.js')}}"></script>
<script src="{{asset('public/Eshopper/js/jquery.scrollUp.min.js')}}"></script>
<script src="{{asset('public/Eshopper/js/price-range.js')}}"></script>
<script src="{{asset('public/Eshopper/js/jquery.prettyPhoto.js')}}"></script>
<script src="{{asset('public/Eshopper/js/main.js')}}"></script>
</body>
</html>
<script>
    function  remove_background(product_id){
        for(var count=1;count<=5;count++){
            $("#"+product_id+'-'+count).css('color','#ccc');
        }
    }
    $(document).on('mouseenter','.rating',function (){
        var index =$(this).data('index');
        var product_id=$(this).data('product_id');

        remove_background(product_id);
        for(var count=1;count<=index;count++){
            $("#"+product_id+'-'+count).css('color','#ffcc00');
        }
    });
    $(document).on('mouseleave','.rating',function (){
        var index =$(this).data('index');
        var product_id=$(this).data('product_id');
        var rating=$(this).data('rating');
        remove_background(product_id);
        for(var count=1;count<=rating;count++){
            $("#"+product_id+'-'+count).css('color','#ffcc00');
        }
    });
    $(document).on('click','.rating',function (){
        var index =$(this).data('index');
        var product_id=$(this).data('product_id');
        $.ajax({
            url:"{{asset('rating')}}",
            method:'get',
            data:{index:index,product_id:product_id},
            success:function (data){
                if(data=="done"){
                    alert("Bạn đã đánh giá "+index+" trên 5");
                }else {
                    alert("Lỗi đánh giá");
                }
            }
        })
    });

</script>

<script>
    $(document).ready(function (){

        load_coment();

        function load_coment(){
            var id_news=$('#news_id').val();
            $.ajax({
                url:"{{asset('loadcomment')}}",
                method:'get',
                data:{id_news:id_news},
                success:function (data){$('#show_comment').html(data);
                }
            })
        }
        $('#send_comment').click(function (){
            var name=$('#name').val();
            var id_message=$('#id_message').val();
            var id_news=$('#news_id').val();

            $.ajax({
                url:"{{asset('addcomment')}}",
                method:'get',
                data:{name:name,id_message:id_message,id_news:id_news},
                success:function (data){
                    $('#notice').html('<p class="alert-success alert">Bình luận thành công</p>');
                    load_coment();
                    $('#name').val('');

                    $('#id_message').val('');


                }
            });
        });

    });
</script>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v11.0&appId=210227564450050&autoLogAppEvents=1" nonce="3qBOj9dS"></script>
<style>
    .none{
        display: none;
    }
</style>

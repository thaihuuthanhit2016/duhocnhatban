<!DOCTYPE html>
<html lang="en">
@include('template.header.head_cart')
<body>
@include('template.header.cart')
<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="{{asset('/')}}">Trang chủ</a></li>
                <li class="active">Thanh toán</li>
            </ol>
        </div>
        <!--/breadcrums-->
        <div class="container">
            <div class="table-responsive cart_info">
                <table class="table table-condensed">
                    <thead>
                    <tr class="cart_menu">
                        <td class="image">Sản phẩm</td>
                        <td class="description"></td>
                        <td class="price">Giá</td>
                        <td class="quantity">Số lượng</td>
                        <td class="total">Tổng tiền</td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i=0;
                    ?>
                    @foreach($content as $c)
                        <?php
                        $i++;
                        $check_product=DB::table('tbl_product')->where('id_product',$c->id)->first();
                        //                    dd($check_product);
                        ?>
                        <tr>
                            <td class="cart_product">
                                <a href="{{asset('detail/'.$check_product->product_name_slug)}}"><img src="{{asset('upload/product/'.$c->options->image)}}" width="70px" alt=""></a>
                            </td>
                            <td class="cart_description">
                                <h4><a href="">{{$c->name}}</a></h4>
                                <p>Mã sản phẩm: {{$check_product->code_product}}</p>
                            </td>
                            <td class="cart_price">
                                <p>{{number_format($c->price,0,',','.')}} VNĐ</p>
                            </td>
                            <td class="cart_quantity">
                                <form method="post" action="{{asset('updatecart')}}">
                                    @csrf
                                    <div class="cart_quantity_button">
                                        <label>{{$c->qty}}</label>
                                    </div>
                                </form>
                            </td>
                            <td class="cart_total">
                                <p class="cart_total_price result_{{$i}}"><?php $total=($c->price*$c->qty);echo number_format($total,0,',','.');?> VNĐ</p>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <br>

            <?php

            if(Session::get('id_offer')!='') {
                $total=round(Cart::subtotal())*1000;
                $percent=Session::get('id_offer')/100;
                $percent=($total*$percent);
                $total_percent=$total-$percent;

            }else{

                $total_percent=Session::get('feeship');
            }

?>
        </div>

<div class="col-md-5">
        <form method="post" >
            @csrf
            <div class="payment-optionsss" id="check_payment">

                <div class="clearfix"></div><br>
                <div class="col-md-12" id="bitcoin">
                    <label><h4>Thành tiền:<span id="fee_payment3" style="color:#FE980F;">
                            <?php
                                $fee_ship=Session::get('fee');
                                $fee_ship_=$fee_ship*800/20000;
                                $total=($c->price*$c->qty);
                                ?>
                                <?php echo Cart::subtotal();?> VNĐ</span></h4></label><br>
<?php  Session::put('total_percent',$total);?>

</label><br>
                </div>
                <div class="clearfix"></div><br>
                <div class="col-md-12">
                    <input class="form-control"placeholder="email" name="email" required type="text" value="" ><br>
                    <input class="form-control"placeholder="tên" name="customer_name" required type="text" value="" ><br>
                    <input class="form-control" placeholder="số điện thoại" name="phone" required type="text" value=""><br>
                    <input class="form-control" placeholder="địa chỉ" name="address" required type="text" value=""><br>

                    <textarea name="note" class="form-control" placeholder="Ghi chú  ..."></textarea>

                </div>
                <div class="clearfix"></div><br>
            </div>
            <div class="col-md-3">
                <input type="submit" id='dathang'class="btn-primary btn" value="Đặt hàng">

            </div>
            <div class="clearfix"></div>
            <br>
        </form>
</div>
        <div class="col-md-7">
            <center><h3>Thông tin xe </h3></center>
            @foreach($car as $c)
                <div class="row"style="margin-bottom: 20px">
                    <div class="col-md-3">
                        <img style="padding: 10px" src="{{asset('upload/product/'.$c->product_images)}}" width="90%">
                    </div>
                    <div class="col-md-9">
                        <p>Tên xe:<b>{{$c->product_name}}</b></p>
                        <p>Số điện thoại:<b>{{$c->phone}}</b></p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
<!--/#cart_items-->
<!--/#do_action-->

@extends('admin.master');
@section('content')
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Thêm Danh mục Tin tức
            </header>
            <div class="panel-body">
                <div class="position-center">

                    <form role="form" method="post">
                        @csrf
                        <div class="form-group">
                            @include('admin.errors.error')
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tên Danh mục</label>
                            <input type="text" class="form-control" value="@if(Session::get('name_category')!=null) {{Session::get('name_category')}} @endif" name="category_product_name" id="exampleInputEmail1" placeholder="Tên danh mục">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Mô tả danh mục</label>
                            <textarea name="category_product_desc" class="form-control" id="exampleInputPassword1" >@if(Session::get('category_desc')!=null) {{Session::get('category_desc')}} @endif</textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Ẩn/Hiện</label>
                            <input type="checkbox" value="1" checked name="category_status">
                        </div>
                        <button type="submit" class="btn btn-info">Submit</button>
                    </form>
                </div>

            </div>
        </section>

    </div>

</div>
@endsection
<?php

function showCategories($categories,$parent_id=0,$char=''){
    foreach($categories as $key =>$item){
        $data=DB::table('tbl_category_product')->where('id_category_product',$item->id_category_product)->first();
        if($item->parent_id==$parent_id){
            echo '<option value="'.$data->id_category_product.'">'.$char.$data->category_name.'</option>';
            unset($categories[$key]);
            showCategories($categories,$item->id_category_product,$char=' | --');
        }
    }
}
?>

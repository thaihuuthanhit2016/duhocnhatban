@extends('admin.master')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
Thông tin shop <b>{{Session::get('name_shop')}}   </b>         </header>
            <div class="panel-body">
                <div class="position-center">
                    @if($shop->verifyKYC=='N')
                    <div class="alert-danger alert">
                        <p>Vui lòng cập nhật KYC !</p>
                    </div>
                    @elseif($shop->verifyKYC=='P')
                    <div class="alert-warning alert">
                        <p>Vui lòng đợi chúng tôi đang duyệt KYC của bạn !</p>
                    </div>
                    @elseif($shop->verifyKYC=='Y')

                    <div class="alert-success alert">
                        <p>KYC shop của bạn đã hợp lệ!</p>
                    </div>
                    @else
                    <div class="alert-info alert">
                        <p>Vui lòng cập nhật lại KYC !</p>
                    </div>
                    @endif
                    <form role="form" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            @include('admin.errors.error')
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tên shop</label>
                            <input type="text" class="form-control" value="{{$shop->name_shop}}" name="name_shop" id="exampleInputEmail1" placeholder="Tên Shop">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Ví shop</label>
                            <input type="text" class="form-control" value="{{$shop->WALLET}}" @if($shop->WALLET!=null) readonly @endif id="address_wallet33" placeholder="Địa chỉ">
                            <p class="title_1" style="cursor: pointer;color:#FE980F " id="check" >Kiểm tra ví</p>
                            <p class="title_1 none" style="cursor: pointer;color:#FE980F " id="check2" >Đang xử lý...</p>
                            <p id="kq" style="color:red;"></p>
                            <p id="kq2" style="color:blue;"></p>
                            Note: Nếu Chưa có ví BITCOINSJC, Vui lòng đăng ký tại <a  target="_blank" href="http://localhost/coindemo_1/register" style="color:#FE980F;">Đăng ký</a>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Địa chỉ</label>
                            <input type="text" class="form-control" value="{{$shop->address}}" name="address" id="exampleInputEmail1" placeholder="Địa chỉ">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">CMND/CCCD (Mặt trước)</label>
                            <input type="file" class="form-control"  name="images1" id="exampleInputEmail1" >
                        </div>
                        @if($shop->images1!=null)
                        <div class="form-group">
                            <img src="{{asset('upload/shop/'.$shop->images1)}}" width="50px"  >
                        </div>
                        @else
                            <div class="form-group">
                                <img src="{{asset('upload/shop/cmnd_mt.jpg')}}" width="50px"  >
                            </div>

                        @endif
                        <div class="form-group">
                            <label for="exampleInputEmail1">CMND/CCCD (Mặt sau)</label>
                            <input type="file" class="form-control"  name="images2" id="exampleInputEmail1" >
                        </div>

                        @if($shop->images2!=null)
                            <div class="form-group">
                                <img src="{{asset('upload/shop/'.$shop->images2)}}" width="50px"  >
                            </div>
                        @else
                            <div class="form-group">
                                <img src="{{asset('upload/shop/cmnd_ms.jpg')}}" width="50px"  >
                            </div>

                        @endif
                        <div class="form-group">
                            <label for="exampleInputEmail1">Logo Shop</label>
                            <input type="file" class="form-control"  name="images3" id="exampleInputEmail1" >
                        </div>

                        @if($shop->images3!=null)
                            <div class="form-group">
                                <img src="{{asset('upload/shop/'.$shop->images3)}}" width="50px"  >
                            </div>
                        @else
                            <div class="form-group">
                                <img src="{{asset('upload/shop/logo.png')}}" width="50px"  >
                            </div>

                        @endif
                        @if($shop->verifyKYC=='N'||$shop->verifyKYC=='A')

                        <button type="submit" class="btn btn-info">Submit</button>
                        @endif
                    </form>
                </div>

            </div>
        </section>

    </div>

</div>
@endsection
<?php

function showCategories($categories,$parent_id=0,$char=''){
    foreach($categories as $key =>$item){
        if($item->parent_id==$parent_id){
            echo '<option value="'.$item->id_category_product.'">'.$char.$item->category_name.'</option>';
            unset($categories[$key]);
            showCategories($categories,$item->id_category_product,$char=' | --');
        }
    }
}
?>
<style>
    .none{
        display: none;
    }
</style>

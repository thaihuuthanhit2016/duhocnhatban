@extends('admin.master');
@section('content')
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
            Cập nhật
            </header>
            <div class="panel-body">
                <div class="position-center">
                    <form role="form" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="exampleInputEmail1">Trạng thái</label>
                            <select class="form-control" name="status_order">

                                @if($bill->status==0)
                                <option value="1">Đang giao  hàng</option>
                                    <option value="3">Hủy Đơn hàng</option>
                                @elseif($bill->status==1)
                                <option value="2">Giao hàng thành công</option>
                                <option value="3">Hủy Đơn hàng</option>
                                @elseif($bill->status==2)

                                    <option value="2">Giao hàng thành công</option>
                                @else
                                    <option value="3">Đã hủy</option>
                                @endif
                            </select>
                        </div>
                        <button type="submit" class="btn btn-info">Submit</button>

                    </form>
                </div>

            </div>
        </section>

    </div>

</div>
@endsection
<?php

function showCategories($categories,$parent_id=0,$char=''){
    foreach($categories as $key =>$item){
        if($item->parent_id==$parent_id){
            echo '<option value="'.$item->id_category_product.'">'.$char.$item->category_name.'</option>';
            unset($categories[$key]);
            showCategories($categories,$item->id_category_product,$char=' | --');
        }
    }
}
?>

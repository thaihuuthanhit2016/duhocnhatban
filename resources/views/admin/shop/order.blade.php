@extends('admin.master');
@section('content')
    <div class="table-agile-info">
        <div class="panel panel-default">
            <div class="panel-heading">
                Danh sách Đơn hàng
            </div>
            <div class="row w3-res-tb">
                <div class="col-sm-5 m-b-xs">
                    <select class="input-sm form-control w-sm inline v-middle">
                        <option value="0">Bulk action</option>
                        <option value="1">Delete selected</option>
                        <option value="2">Bulk edit</option>
                        <option value="3">Export</option>
                    </select>
                    <button class="btn btn-sm btn-default">Apply</button>
                </div>
                <div class="col-sm-4">
                </div>
                <div class="col-sm-3">
                    <div class="input-group">
                        <input type="text" class="input-sm form-control" placeholder="Search">
                        <span class="input-group-btn">
            <button class="btn btn-sm btn-default" type="button">Go!</button>
          </span>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                @include('admin.errors.error')
<?php
                Session::put('message_error','');
                Session::put('product_code',"");
                Session::put('product_name','');
                Session::put('product_price','');
                Session::put('product_price_km','');
                Session::put('product_desc','');
                Session::put('product_content','');

          ?>      <table class="table table-striped b-t b-light">
                    <thead>
                    <tr>
                        <th style="width:20px;">
                            <label class="i-checks m-b-none">
                                <input type="checkbox"><i></i>
                            </label>
                        </th>
                        <th>Tên Khách hàng</th>
                        <th>Tổng tiền</th>
                        <th>Địa chỉ</th>
                        <th>Ngày đặt hàng</th>

                        <th>Trạng thái</th>
                        <th>Thanh toán</th>
                        <th style="width:100px;">Tùy chọn</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($bill as $d)
                        <?php

                        $bill_=DB::table('tbl_bill')->where('id_bill',$d->id_bill)->first();
                        $bill_detail=DB::table('tbl_bill_detail')->where('id_bill',$d->id_bill)->first();
                        $id_shop=DB::table('tbl_product_shop')->where('id_product',$bill_detail->id_product)->first();

                        $customer=DB::table('tbl_customer')->where('id_customer',$bill_->id_customer)->first();
                        $payment=DB::table('tbl_payment')->where('id_bill',$bill_->id_bill)->first();

                        ?>

                    <tr>
                        <td><label class="i-checks m-b-none"><input type="checkbox" name="post[]"><i></i></label></td>
                        <td>{{$customer->customer_name}}</td>
                        <td>{{number_format($bill_->total,0,',','.')}} VNĐ</td>
                        <td>{{$customer->customer_address}} </td>
                        <td>{{$bill_->created}} </td>

                        <td>@if($bill_->status==0) <p class="alert-info alert">Mới đặt hàng</p> @elseif($bill_->status==1) <p class="alert-warning alert"> Đang giao hàng</p>@elseif($bill_->status==2)<p class="alert alert-success"> Đã giao</p>  @else <p class="alert-danger alert">Đã hủy</p>@endif </td>

<td> Chưa thanh toán</td>
                        <td>
                            <a href="{{asset('admin/order/viewordered/'.$bill_->id_bill)}}" class="active" ui-toggle-class=""><i class="fa fa-eye text-success text-active"></i></a> <a href="{{asset('admin/order/edit/'.$bill_->id_bill)}}" class="active" ui-toggle-class=""><i class="fa fa-edit text-warning text-active"></i></a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>





@endsection

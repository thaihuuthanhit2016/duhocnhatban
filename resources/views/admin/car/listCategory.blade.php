@extends('admin.master');
@section('content')
    <div class="table-agile-info">
        <div class="panel panel-default">
            <div class="panel-heading">
                Danh sách Sản phẩm
            </div>
<br>
            <div class="table-responsive">
                @include('admin.errors.error')
                <br>
                <br>
<?php
                Session::put('message_error','');
                Session::put('product_code',"");
                Session::put('product_name','');
                Session::put('product_price','');
                Session::put('product_price_km','');
                Session::put('product_desc','');
                Session::put('product_content','');

          ?>
                <br>

                <table id="example"  border="1" class="display" cellspacing="0" width="100%">

                        <thead>
                        <tr>
                            <th>Tên Xe</th>
                            <th>Hình ảnh</th>
                            <th>Số điện thoại</th>

                            <th style="width:100px;">Tùy chọn</th>
                        </tr>
                        </thead>
                        <tbody id="kq">
                        @foreach($data as $d)


                            <tr >
                                <td>{{$d->product_name}}</td>
                                <td><img  src="{{asset('upload/product/'.$d->product_images)}}" width="50px" ></td>
                                <td>{{$d->phone}}</td>


                                <td>
                                    <a href="{{asset('admin/car/view/'.$d->id_car)}}" class="active" ui-toggle-class=""><i class="fa fa-eye text-success text-active"></i></a>&nbsp;&nbsp;&nbsp;<a href="{{asset('admin/car/edit/'.$d->id_car)}}" class="active" ui-toggle-class=""><i class="fa fa-pencil-square-o text-success text-active"></i></a>&nbsp;&nbsp;&nbsp; <a href="{{asset('admin/car/del/'.$d->id_car)}}" onclick="confirm('Bạn có chắc xóa không?')"> <i class="fa fa-times text-danger text"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script src='https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js'></script>
    <script src="{{asset('public/admin/dist_datatable/script.js')}}"></script>

@endsection

<script>
    $(document).ready(function (){
        $('#phantrang').change(function (){
            var phantrang=$(this).val();

            $.ajax({
                url : "{{asset('admin/product/list_/')}}", // gửi ajax đến file result.php
                type : "get", // chọn phương thức gửi là get
                dateType:"text", // dữ liệu trả về dạng text
                data : { // Danh sách các thuộc tính sẽ gửi đi
                    phantrang : $('#phantrang').val()
                },
                success : function (result){
                    // Sau khi gửi và kết quả trả về thành công thì gán nội dung trả về
                    // đó vào thẻ div có id = result
                    $('#kq').html(result);
                }
            });
        });
       $("#search").keyup(function (){
           var search=$("#search").val();
           $.ajax({
               url : "{{asset('admin/product/serachproduct')}}", // gửi ajax đến file result.php
               type : "get", // chọn phương thức gửi là get
               dateType:"text", // dữ liệu trả về dạng text
               data : { // Danh sách các thuộc tính sẽ gửi đi
                   search : $('#search').val()
               },
               success : function (result){
                   // Sau khi gửi và kết quả trả về thành công thì gán nội dung trả về
                   // đó vào thẻ div có id = result
                   $('#kq').html(result);
               }
           });
       }) ;
    });
</script>


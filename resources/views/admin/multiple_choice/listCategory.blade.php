@extends('admin.master');
@section('content')
    <div class="table-agile-info">
        <div class="panel panel-default">
            <div class="panel-heading">
                Danh sách Sản phẩm
            </div>
            <div class="row w3-res-tb">
                <div class="col-sm-5 m-b-xs">
                    <select class="input-sm form-control w-sm inline v-middle">
                        <option value="0">Bulk action</option>
                        <option value="1">Delete selected</option>
                        <option value="2">Bulk edit</option>
                        <option value="3">Export</option>
                    </select>
                    <button class="btn btn-sm btn-default">Apply</button>
                </div>
                <div class="col-sm-4">
                </div>
                <div class="col-sm-3">
                    <div class="input-group">
                        <input type="text" class="input-sm form-control" placeholder="Search">
                        <span class="input-group-btn">
            <button class="btn btn-sm btn-default" type="button">Go!</button>
          </span>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                @include('admin.errors.error')
<?php
                Session::put('message_error','');
                Session::put('product_code',"");
                Session::put('product_name','');
                Session::put('product_price','');
                Session::put('product_price_km','');
                Session::put('product_desc','');
                Session::put('product_content','');

          ?>      <table class="table table-striped b-t b-light">
                    <thead>
                    <tr>
                        <th style="width:20px;">
                            <label class="i-checks m-b-none">
                                <input type="checkbox"><i></i>
                            </label>
                        </th>
                        <th>Tiêu đề</th>

                        <th>Loại</th>
                        <th style="width:100px;">Tùy chọn</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $d)

                    <tr>
                        <td><label class="i-checks m-b-none"><input type="checkbox" name="post[]"><i></i></label></td>
                        <td>{{$d->title}}</td>
                        <td>@if($d->type==0) Chính sách @elseif($d->type==1) Hướng dẫn @elseif($d->type==2) Khuyến mãi @else Dịch vụ @endif</td>

                        <td>
                            <a href="{{asset('admin/pages/edit/'.$d->id_pages)}}" class="active" ui-toggle-class=""><i class="fa fa-pencil-square-o text-success text-active"></i></a>&nbsp;&nbsp;&nbsp; <a href="{{asset('admin/product/del/'.$d->id_pages)}}" onclick="confirm('Bạn có chắc xóa không?')"> <i class="fa fa-times text-danger text"></i></a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{$data->links('admin.paginate')}}
        </div>
    </div>

@endsection

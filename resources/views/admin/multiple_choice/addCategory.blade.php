@extends('admin.master');
@section('content')
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Thêm Câu hỏi
            </header>
            <div class="panel-body">
                <div class="position-center">

                    <form role="form" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            @include('admin.errors.error')
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Câu hỏi</label>
                            <input type="text" class="form-control"  name="product_name" id="exampleInputEmail1" placeholder="Tên ">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Đáp án A</label>
                            <textarea name=option_a" class="form-control"  ></textarea>


                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Đáp án B</label>
                            <textarea name=option_b" class="form-control"  ></textarea>


                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Đáp án C</label>
                            <textarea name=option_c" class="form-control"  ></textarea>


                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Đáp án D</label>
                            <textarea name=option_d" class="form-control"  ></textarea>


                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Đáp án đúng</label>
                            <textarea name=answer" class="form-control"  ></textarea>


                        </div>
                        <button type="submit" class="btn btn-info">Submit</button>
                    </form>
                </div>

            </div>
        </section>

    </div>

</div>
@endsection
<?php

function showCategories($categories,$parent_id=0,$char=''){
    foreach($categories as $key =>$item){
        if($item->parent_id==$parent_id){
            echo '<option value="'.$item->id_category_product.'">'.$char.$item->category_name.'</option>';
            unset($categories[$key]);
            showCategories($categories,$item->id_category_product,$char=' | --');
        }
    }
}
?>

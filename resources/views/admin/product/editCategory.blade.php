@extends('admin.master')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Sửa Sản phẩm
                </header>
                <br>
                @include('admin.errors.error')

                <div class="panel-body">
                    <div class="position-center">

                        <form role="form" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                @include('admin.errors.error')
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tên Sản phẩm</label>
                                <input type="text" class="form-control" value="{{$product->product_name}}" name="product_name" id="exampleInputEmail1" placeholder="Tên ">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Giá Sản phẩm</label>
                                <input type="text" class="form-control" value="{{$product->product_price}}" name="product_price" id="exampleInputEmail1" placeholder="Tên ">
                            </div>
                            <?php
                                $soluong=DB::table('tbl_depot')->where('id_product',$product->id_product)->first();

                            ?>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Hình ảnh Sản phẩm</label>
                                <input type="file" class="form-control" name="product_images">
                            </div>
                            <div class="form-group">

                                <img src="{{asset('upload/product/'.$product->product_images)}}" width="50%">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Mô tả Sản phẩm</label>
                                <textarea name="product_desc" class="form-control" id="exampleInputPassword1" >{{$product->product_desc}}</textarea>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Tag Sản phẩm</label>
                                <input type="text"  class="form-control" value="{{$product->product_tag}}"  name="product_tags" data-role="tagsinput"
                                       >
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Nội dung Sản phẩm</label>
                                <textarea name="product_content" class="form-control" id="product_content" >{{$product->product_content}}</textarea>


                            </div>
                            <button type="submit" class="btn btn-info">Submit</button>
                        </form>
                    </div>

                </div>
            </section>

        </div>

    </div>
@endsection
<?php

function showCategories($categories,$parent_id=0,$char=''){
    foreach($categories as $key =>$item){
        if($item->parent_id==$parent_id){
            echo '<option value="'.$item->id_category_product.'">'.$char.$item->category_name.'</option>';
            unset($categories[$key]);
            showCategories($categories,$item->id_category_product,$char=' | --');
        }
    }
}
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script type="text/javascript" src="{{asset('public/admin/js/bootstrap-tagsinput.js')}}"></script>

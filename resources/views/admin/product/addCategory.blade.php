@extends('admin.master');
@section('content')
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Thêm Sản phẩm
            </header>
            <div class="panel-body">
                <div class="position-center">

                    <form role="form" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            @include('admin.errors.error')
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tên Sản phẩm</label>
                            <input type="text" class="form-control" value="@if(Session::get('product_name')!=null) {{Session::get('product_name')}} @endif" name="product_name" id="exampleInputEmail1" placeholder="Tên ">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Giá Sản phẩm</label>
                            <input type="text" class="form-control" value="@if(Session::get('product_price')!=null) {{Session::get('product_price')}} @endif" name="product_price" id="exampleInputEmail1" placeholder="Tên ">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Hình ảnh Sản phẩm</label>
                            <input type="file" class="form-control  images-preview" name="product_images" onchange="previewFile(this)">
                            <div class="clearfix"></div>
                            <br>
                            <img src="{{asset('upload/avatar/no-image-800x600.png')}}" id="previewimg" >
                            <div class="clearfix"></div>
                            <br>
                        </div>
<style>
    #previewimg{
        width: 200px!important;
        height:  auto!important;

    }
    .none{
        display: none;
    }
</style>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Mô tả Sản phẩm</label>
                            <textarea name="product_desc" class="form-control" id="exampleInputPassword1" >@if(Session::get('product_desc')!=null) {{Session::get('product_desc')}} @endif</textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Nội dung Sản phẩm</label>
                            <textarea name="product_content" class="form-control" id="product_content" >@if(Session::get('product_content')!=null) {{Session::get('product_content')}} @endif</textarea>


                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Tag Sản phẩm</label>
                            <input type="text" value="" class="form-control"  name="product_tags" data-role="tagsinput" placeholder="Add tags" />
                        </div>
                        <button type="submit" class="btn btn-info">Submit</button>
                    </form>
                </div>

            </div>
        </section>

    </div>

</div>
@endsection
<?php

function showCategories($categories,$parent_id=0,$char=''){
    foreach($categories as $key =>$item){
        if($item->parent_id==$parent_id){
            echo '<option value="'.$item->id_category_product.'">'.$char.$item->category_name.'</option>';
            unset($categories[$key]);
            showCategories($categories,$item->id_category_product,$char=' | --');
        }
    }
}
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script type="text/javascript" src="{{asset('public/admin/js/bootstrap-tagsinput.js')}}"></script>


<script>
    function previewFile(input){
        var file=$('.images-preview').get(0).files[0];
        if(file){
            var  reader=new  FileReader();
            reader.onload=function (){
                $("#previewimg").attr('src',reader.result);
            }
            reader.readAsDataURL(file);
        }
    }
</script>

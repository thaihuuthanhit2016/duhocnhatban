@extends('admin.master');
@section('content')
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Thêm Khuyến mãi
            </header>
            <div class="panel-body">
                <div class="position-center">

                    <form role="form" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tên Khuyến mãi</label>
                            <input type="text" class="form-control" value="@if(Session::get('title')!=null) {{Session::get('title')}} @endif" name="title" id="exampleInputEmail1" placeholder="Tên khuyến mãi">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Mã Khuyến mãi</label>
                            <input type="text" class="form-control" value="@if(Session::get('code_offer')!=null) {{Session::get('code_offer')}} @endif" name="code_offer" id="exampleInputEmail1" placeholder="Mã khuyến mãi">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Phần trăm giảm giá</label>
                            <input type="text" class="form-control" value="" name="percent" id="exampleInputEmail1" placeholder="Mã khuyến mãi">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Số lượng</label>
                            <input type="text" class="form-control" value="" name="number" id="exampleInputEmail1" placeholder="Mã khuyến mãi">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Loại khuyến mãi</label>
                            <select class="form-control">
                                <option value="1">BitcoinSJC</option>
                                <option value="2">Shop</option>
                                <option value="3">Shop</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Mô tả </label>
                            <textarea name="content" class="form-control" id="exampleInputPassword1" >@if(Session::get('content')!=null) {{Session::get('content')}} @endif</textarea>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Từ ngày</label>
                            <input type="text" class="form-control" name="from_date" id="exampleInputEmail1" placeholder="Từ ngày">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Đến ngày</label>
                            <input type="text" class="form-control"  name="end_date" id="exampleInputEmail1" placeholder="Đến ngày">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Cho tài khoản</label>
                            <select class="form-control" name="user">
                                <option value="0">Không chọn</option>
                                @foreach($customer as $c)
                                    <?php
                                     $customer=DB::table('tbl_customer')->where('id_customer',$c->id_customer)->first();
                                    ?>
                                    <option value="{{$c->id_customer}}">{{$customer->customer_email }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button type="submit" class="btn btn-info">Submit</button>
                    </form>
                </div>

            </div>
        </section>

    </div>

</div>
@endsection
<?php

function showCategories($categories,$parent_id=0,$char=''){
    foreach($categories as $key =>$item){
        if($item->parent_id==$parent_id){
            echo '<option value="'.$item->id_category_product.'">'.$char.$item->category_name.'</option>';
            unset($categories[$key]);
            showCategories($categories,$item->id_category_product,$char=' | --');
        }
    }
}
?>

@extends('admin.master');
@section('content')
    <div class="table-agile-info">
        <div class="panel panel-default">
            <div class="panel-heading">
                Danh sách mã khuyến mãi
            </div>
            <div class="row w3-res-tb">
                <div class="col-sm-5 m-b-xs">
                    <select class="input-sm form-control w-sm inline v-middle">
                        <option value="0">Bulk action</option>
                        <option value="1">Delete selected</option>
                        <option value="2">Bulk edit</option>
                        <option value="3">Export</option>
                    </select>
                    <button class="btn btn-sm btn-default">Apply</button>
                </div>
                <div class="col-sm-4">
                </div>
                <div class="col-sm-3">
                    <div class="input-group">
                        <input type="text" class="input-sm form-control" placeholder="Search">
                        <span class="input-group-btn">
            <button class="btn btn-sm btn-default" type="button">Go!</button>
          </span>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <?php

                Session::put('message_error','');
                Session::put('title','');
                Session::put('code_offer','');
                Session::put('content','');
                ?>
                <table class="table table-striped b-t b-light">
                    <thead>
                    <tr>
                        <th style="width:20px;">
                            <label class="i-checks m-b-none">
                                <input type="checkbox"><i></i>
                            </label>
                        </th>
                        <th>Tên Khuyến mãi</th>
                        <th>Số lượng</th>

                        <th>Từ ngày</th>
                        <th>Đến ngày</th>
                        @if(Session::get('admin_id'))
                        <th>Shop</th>
                        @endif

                        <th>Khách hàng</th>

                        <th style="width:30px;"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(Session::get('admin_id'))
                    @foreach($data as $d)
<?php
$today = date('d-m-Y');

$week = strtotime(date("Y-m-d", strtotime($today)) . " +6 day");
$offer_shop=DB::table('tbl_offer_shop')->where('id_offer',$d->id_offer)->first();
$offer_customer=DB::table('tbl_offer_customer')->where('id_offer',$d->id_offer)->first();
if($offer_shop!=null){

    $shop=DB::table('tbl_shop')->where('id_shop_user',$offer_shop->id_shop)->first();

}
else{
    $shop=null;
}
if($offer_customer!=null){

    $customer=DB::table('tbl_customer')->where('id_customer',$offer_customer->id_customer)->first();

}else{
    $customer=null;
}

?>
                    <tr @if(strtotime(date('d-m-Y'))>=strtotime($d->end_date))  style="background: red;color: white" @endif @if(strtotime(date('d-m-Y'))<=strtotime($d->end_date))  style="background: green;color: white" @endif>
                        <td><label class="i-checks m-b-none"><input type="checkbox" name="post[]"><i></i></label></td>
                        <td>{{$d->title}}</td>
                        <td>{{$d->number}}</td>
                        <td>{{$d->from_date}}</td>
                        <td>{{$d->end_date}}</td>
                        @if(Session::get('admin_id'))
                            @if($shop!=null)
                        <td>{{$shop->name_shop}}</td>
                            @else
                                <td>None</td>

                            @endif
                        @endif
                        @if($customer!=null)
                        <td>{{$customer->customer_email}}</td>
                        @else
                            <td>None</td>

                        @endif
                        <td>
                            <a href="{{asset('admin/offer/edit/'.$d->id_offer)}}" class="active" ui-toggle-class=""><i class="fa fa-pencil-square-o text-success text-active"></i></a> <a href="{{asset('admin/offer/del/'.$d->id_offer)}}" onclick="confirm('Bạn có chắc xóa không?')"> <i class="fa fa-times text-danger text"></i></a>
                        </td>
                    </tr>
                    @endforeach
                    @else
                        @foreach($data as $c)
                            <?php
                                $d=DB::table('tbl_offer')->where('id_offer',$c->id_offer)->first();
                                $ofer=DB::table('tbl_offer_customer')->where('id_offer',$c->id_offer)->first();

                                $dd=DB::table('tbl_customer')->where('id_customer',$ofer->id_customer)->first();

                            ?>
                            <tr @if(strtotime(date('d-m-Y'))>=strtotime($d->end_date))  style="background: red;color: white" @endif @if(strtotime(date('d-m-Y'))<=strtotime($d->end_date))  style="background: green;color: white" @endif>
                                <td><label class="i-checks m-b-none"><input type="checkbox" name="post[]"><i></i></label></td>
                                <td>{{$d->title}}</td>
                                <td>{{$d->number}}</td>
                                <td>{{$d->from_date}}</td>
                                <td>{{$d->end_date}}</td>
                                <td>{{$dd->customer_email}}</td>
                                <td>
                                    <a href="{{asset('admin/offer/edit/'.$d->id_offer)}}" class="active" ui-toggle-class=""><i class="fa fa-pencil-square-o text-success text-active"></i></a> <a href="{{asset('admin/offer/del/'.$d->id_offer)}}" onclick="confirm('Bạn có chắc xóa không?')"> <i class="fa fa-times text-danger text"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            <footer class="panel-footer">
                <div class="row">

                    <div class="col-sm-7 text-right text-center-xs">
                        {{$data->links('admin.paginate')}}
                    </div>
                </div>
            </footer>
        </div>
    </div>

@endsection

@extends('admin.master');
@section('content')
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
            Rút tiền
            </header>
            <div class="panel-body">
                <div class="position-center">

                    <br>
                    @include('admin.errors.error')
                    <div class="clearfix"></div><br>

                    <form role="form" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="exampleInputEmail1">Số tiền cần rút</label>
                            <input type="number"class="form-control" name="Amount" min="500">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Loại Tiền tệ </label>
                            <select class="form-control" name="type_money">
                                <option value="1">VNĐ</option>
                                <option value="3">BITCOINSJC</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Ví của shop</label>
                            <input readonly name="address" class="form-control" value="{{$shop->WALLET}}" >
                        </div>
                        <button type="submit" class="btn btn-info">Submit</button>

                    </form>
                </div>

            </div>
        </section>

    </div>

</div>
@endsection
<?php

function showCategories($categories,$parent_id=0,$char=''){
    foreach($categories as $key =>$item){
        if($item->parent_id==$parent_id){
            echo '<option value="'.$item->id_category_product.'">'.$char.$item->category_name.'</option>';
            unset($categories[$key]);
            showCategories($categories,$item->id_category_product,$char=' | --');
        }
    }
}
?>

@extends('admin.master')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Thông tin shop <b>{{Session::get('admin_name')}}   </b>         </header>
                <div class="panel-body">
                    <div class="position-center">
                        @if($shipper->verifyKYC=='N')
                            <div class="alert-danger alert">
                                <p>Vui lòng cập nhật KYC !</p>
                            </div>
                        @elseif($shipper->verifyKYC=='P')
                            <div class="alert-warning alert">
                                <p>Vui lòng đợi chúng tôi đang duyệt KYC của bạn !</p>
                            </div>
                        @elseif($shipper->verifyKYC=='Y')

                            <div class="alert-success alert">
                                <p>KYC shop của bạn đã hợp lệ!</p>
                            </div>
                        @else
                            <div class="alert-info alert">
                                <p>Vui lòng cập nhật lại KYC !</p>
                            </div>
                        @endif
                        <form role="form" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                @include('admin.errors.error')
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Username </label>
                                <input type="text" class="form-control" readonly value="{{$shipper->username}}" name="name_shop" id="exampleInputEmail1" placeholder="Tên Shop">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Ví </label>
                                <input type="text" class="form-control" value="{{$shipper->WALLET}}" @if($shipper->WALLET!=null) readonly @endif id="address_wallet33" placeholder="Địa chỉ">
                                <p class="title_1" style="cursor: pointer;color:#FE980F " id="check" >Kiểm tra ví</p>
                                <p class="title_1 none" style="cursor: pointer;color:#FE980F " id="check2" >Đang xử lý...</p>
                                <p id="kq" style="color:red;"></p>
                                <p id="kq2" style="color:blue;"></p>
                                Note: Nếu Chưa có ví BITCOINSJC, Vui lòng đăng ký tại <a  target="_blank" href="http://localhost/coindemo_1/register" style="color:#FE980F;">Đăng ký</a>
                            </div>
                            <div class="form-group">

                                <label for="Country">Tỉnh Thành phố</label>
                                <select id="city"  class="js-states choose city form-control">
                                    <option>Chọn Tỉnh Thành phố</option>

                                    @foreach($city as $c)
                                        <option value="{{$c->matp}}" >{{$c->name_city}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Chọn Quận Huyện</label>
                                <select class="form-control input-sm m-bot-15 choose province" id="province" name="category_id">
                                    <option value="">Chọn Quận Huyện</option>

                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Chọn Xã Phường</label>
                                <select class="form-control input-sm m-bot-15 wards" id="wards" name="category_id">
                                    <option value="">Chọn Xã Phường</option>

                                </select>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">CMND/CCCD (Mặt trước)</label>
                                <input type="file" class="form-control"  name="images1" id="exampleInputEmail1" >
                            </div>
                            @if($shipper->images1!=null)
                                <div class="form-group">
                                    <img src="{{asset('upload/shipper/'.$shipper->images1)}}" width="50px"  >
                                </div>
                            @else
                                <div class="form-group">
                                    <img src="{{asset('upload/shop/cmnd_mt.jpg')}}" width="50px"  >
                                </div>

                            @endif
                            <div class="form-group">
                                <label for="exampleInputEmail1">CMND/CCCD (Mặt sau)</label>
                                <input type="file" class="form-control"  name="images2" id="exampleInputEmail1" >
                            </div>

                            @if($shipper->images2!=null)
                                <div class="form-group">
                                    <img src="{{asset('upload/shipper/'.$shipper->images2)}}" width="50px"  >
                                </div>
                            @else
                                <div class="form-group">
                                    <img src="{{asset('upload/shop/cmnd_ms.jpg')}}" width="50px"  >
                                </div>

                            @endif
                            <div class="form-group">
                                <label for="exampleInputEmail1">Avatar</label>
                                <input type="file" class="form-control"  name="images3" id="exampleInputEmail1" >
                            </div>

                            @if($shipper->images3!=null)
                                <div class="form-group">
                                    <img src="{{asset('upload/shipper/'.$shipper->images3)}}" width="50px"  >
                                </div>
                            @else
                                <div class="form-group">
                                    <img src="{{asset('upload/avatar/avatar.jpg')}}" width="50px"  >
                                </div>

                            @endif
                            @if($shipper->verifyKYC=='N'||$shipper->verifyKYC=='A')

                                <button type="submit" class="btn btn-info">Submit</button>
                            @endif
                        </form>
                    </div>

                </div>
            </section>

        </div>

    </div>
    <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/chosen/1.7.0/chosen.jquery.min.js'></script>

    <script>
        // $("#city").chosen({no_results_text: "Oops, nothing found!"});
        // $("#province").chosen({no_results_text: "Oops, nothing found!"});
        $(document).ready(function (){

            $(".choose").change(function (){
                var action=$(this).attr('id');
                var ma_id=$(this).val();
                var ma_tp=$(this).val();
                var result='';

                if(action=='city'){
                    result="province";
                }else {
                    result="wards";
                }

                $.ajax({
                    url:"{{asset('admin/delivery/province')}}",
                    method:'Get',
                    data:{ma_id:ma_id,ma_tp:ma_tp,action:action},
                    success:function (data){

                        $('#'+result).html(data);
                    }
                });
            });
            ;
        });
    </script>
@endsection
<?php

function showCategories($categories,$parent_id=0,$char=''){
    foreach($categories as $key =>$item){
        if($item->parent_id==$parent_id){
            echo '<option value="'.$item->id_category_product.'">'.$char.$item->category_name.'</option>';
            unset($categories[$key]);
            showCategories($categories,$item->id_category_product,$char=' | --');
        }
    }
}
?>
<style>
    .none{
        display: none;
    }
</style>

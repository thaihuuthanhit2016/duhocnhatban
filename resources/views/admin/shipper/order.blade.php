@extends('admin.master');
@section('content')
    <div class="table-agile-info">
        <div class="panel panel-default">
            <div class="panel-heading">
                Danh sách Đơn hàng
            </div>
            <div class="row w3-res-tb">
                <div class="col-sm-5 m-b-xs">
                    <select class="input-sm form-control w-sm inline v-middle">
                        <option value="0">Bulk action</option>
                        <option value="1">Delete selected</option>
                        <option value="2">Bulk edit</option>
                        <option value="3">Export</option>
                    </select>
                    <button class="btn btn-sm btn-default">Apply</button>
                </div>
                <div class="col-sm-4">
                </div>
                <div class="col-sm-3">
                    <div class="input-group">
                        <input type="text" class="input-sm form-control" placeholder="Search">
                        <span class="input-group-btn">
            <button class="btn btn-sm btn-default" type="button">Go!</button>
          </span>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                @include('admin.errors.error')
<?php
                Session::put('message_error','');
                Session::put('product_code',"");
                Session::put('product_name','');
                Session::put('product_price','');
                Session::put('product_price_km','');
                Session::put('product_desc','');
                Session::put('product_content','');

          ?>      <table class="table table-striped b-t b-light">
                    <thead>
                    <tr>
                        <th style="width:20px;">
                            <label class="i-checks m-b-none">
                                <input type="checkbox"><i></i>
                            </label>
                        </th>
                        <th>Tên Khách hàng</th>
                        <th>Tổng tiền</th>
                        <th>Địa chỉ</th>
                        <th>Ngày đặt hàng</th>
                        @if(Session::get('admin_id'))

                        <th>Shop</th>
                        @endif
                        <th>Trạng thái</th>
                        <th>Thanh toán</th>
                        <th style="width:100px;">Tùy chọn</th>
                    </tr>
                    </thead>
                    <tbody>
@if(Session::get('admin_id'))
                    @foreach($bill as $d)
                        <?php

                        $bill_=DB::table('tbl_bill')->where('id_bill',$d->id_bill)->first();
                        $bill_detail=DB::table('tbl_bill_detail')->where('id_bill',$d->id_bill)->first();
                        $id_shop=DB::table('tbl_product_shop')->where('id_product',$bill_detail->id_product)->first();
                        $shop=DB::table('tbl_shop')->where('id_shop_user',$id_shop->id_shop)->first();
                        $customer=DB::table('tbl_customer')->where('id_customer',$bill_->id_customer)->first();
                        $payment=DB::table('tbl_payment')->where('id_bill',$bill_->id_bill)->first();

                        ?>

                    <tr>
                        <td><label class="i-checks m-b-none"><input type="checkbox" name="post[]"><i></i></label></td>
                        <td>{{$customer->customer_name}}</td>
                        @if($payment->payment_method==1)
                        <td>{{number_format($bill_->total,0,',','.')}} VNĐ</td>
                        <td>{{$customer->customer_address}} </td>
                        <td>{{$bill_->created}} </td>

                        <td>{{$shop->name_shop}} </td>
                        <td>@if($bill_->status==0) <p class="alert-info alert">Mới đặt hàng</p> @elseif($bill_->status==1) <p class="alert-warning alert"> Đang giao hàng</p>@elseif($bill_->status==2)<p class="alert alert-success"> Đã giao</p>  @else <p class="alert-danger alert">Đã hủy</p>@endif </td>
                        @else

                            <td>{{number_format($bill_->total,0,',','.')}} BitCoinSJC</td>
                            <td>{{$customer->customer_address}} </td>
                            <td>{{$bill_->created}} </td>

                            <td>{{$shop->name_shop}} </td>

                            <td>@if($bill_->status==0) <p class="alert-info alert">Mới đặt hàng</p> @elseif($bill_->status==1) <p class="alert-warning alert"> Đang giao hàng</p>@elseif($bill_->status==2)<p class="alert alert-success"> Đã giao</p>  @else <p class="alert-danger alert">Đã hủy</p>@endif </td>
                        @endif
<td>@if($payment->payment_status==0) Chưa thanh toán @else Đã thanh toán @endif</td>
                        <td>
                            <a href="{{asset('admin/order/viewordered/'.$bill_->id_bill)}}" class="active" ui-toggle-class=""><i class="fa fa-eye text-success text-active"></i></a>
                        </td>
                    </tr>
                    @endforeach
@else
    <?php $i=0;?>
    @foreach($bill as $d)
        <?php

        $bill_=DB::table('tbl_bill_detail')->where('id_bill',$d->id_bill)->first();
        $bill_detail=DB::table('tbl_bill')->where('id_bill',$d->id_bill)->first();
        $customer=DB::table('tbl_customer')->where('id_customer',$bill_detail->id_customer)->first();
        $payment=DB::table('tbl_payment')->where('id_bill',$bill_detail->id_bill)->first();
        $shop=DB::table('tbl_product_shop')->where('id_product',$bill_->id_product)->where('id_shop',Session::get('id_shop_user'))->first();
        if($shop!=null){
$i++;
        ?>

        <tr>
            <td><label class="i-checks m-b-none"><input type="checkbox" name="post[]"><i></i></label></td>
            <td>{{$customer->customer_name}}</td>
            @if($payment->payment_method==1)
                <td>{{number_format($bill_detail->total,0,',','.')}} VNĐ</td>
                <td>{{$customer->customer_address}} </td>
                <td>{{$bill_detail->created}} </td>

                <td>@if($bill_detail->status==0) <p class="alert-info alert">Mới đặt hàng</p> @elseif($bill_detail->status==1) <p class="alert-warning alert"> Đang giao hàng</p>@elseif($bill_detail->status==2)<p class="alert alert-success"> Đã giao</p>  @else <p class="alert-danger alert">Đã hủy</p>@endif </td>
            @else

                <td>{{number_format($bill_detail->total,0,',','.')}} BitCoinSJC</td>
                <td>{{$customer->customer_address}} </td>
                <td>{{$bill_detail->created}} </td>

                <td>@if($bill_detail->status==0) <p class="alert-info alert">Mới đặt hàng</p> @elseif($bill_detail->status==1) <p class="alert-warning alert"> Đang giao hàng</p>@elseif($bill_detail->status==2)<p class="alert alert-success"> Đã giao</p>  @else <p class="alert-danger alert">Đã hủy</p>@endif </td>
            @endif
            <td>@if($payment->payment_status==0) Chưa thanh toán @else Đã thanh toán @endif</td>
            <td>
                <a href="{{asset('admin/order/viewordered/'.$bill_detail->id_bill)}}" class="active" ui-toggle-class=""><i class="fa fa-eye text-success text-active"></i></a>
            </td>
        </tr>
        <?php }?>
    @endforeach
@endif
                    </tbody>
                </table>

            </div>
            <footer class="panel-footer">
                <div class="row">

                    <div class="col-sm-7 text-right text-center-xs">
                        {{$bill->links('admin.paginate')}}
                    </div>
                </div>
            </footer>
        </div>
    </div>

@endsection

@extends('admin.master');
@section('content')
    <div class="table-agile-info">
        <div class="panel panel-default">
            <div class="panel-heading">
                Chi tiết đơn hàng <b style="color:#fe980F;">{{$bill->code_bill}}</b>
            </div>
            <div class="row w3-res-tb">
                <div class="col-sm-5 m-b-xs">
                    <select class="input-sm form-control w-sm inline v-middle">
                        <option value="0">Bulk action</option>
                        <option value="1">Delete selected</option>
                        <option value="2">Bulk edit</option>
                        <option value="3">Export</option>
                    </select>
                    <button class="btn btn-sm btn-default">Apply</button>
                </div>
                <div class="col-sm-4">
                </div>
                <div class="col-sm-3">
                    <div class="input-group">
                        <input type="text" class="input-sm form-control" placeholder="Search">
                        <span class="input-group-btn">
            <button class="btn btn-sm btn-default" type="button">Go!</button>
          </span>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                @include('admin.errors.error')
                <?php
                Session::put('message_error','');
                Session::put('product_code',"");
                Session::put('product_name','');
                Session::put('product_price','');
                Session::put('product_price_km','');
                Session::put('product_desc','');
                Session::put('product_content','');

                ?>      <table class="table table-striped b-t b-light">
                    <thead>
                    <tr>
                        <th style="width:20px;">
                            <label class="i-checks m-b-none">
                                <input type="checkbox"><i></i>
                            </label>
                        </th>
                        <th>Tên Sản phẩm</th>
                        <th>Mã Sản phẩm</th>
                        <th>Giá Sản phẩm</th>
                        <th>Số lượng</th>
                        <th>Tổng tiền</th>
                        <th>HTTT</th>

                        <th style="width:100px;">Tùy chọn</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($billdetail as $d)
<?php
$product=DB::table('tbl_product')->where('id_product',$d->id_product)->first();
$bill=DB::table('tbl_bill')->where('id_bill',$d->id_bill)->first();
$payment=DB::table('tbl_payment')->where('id_bill',$d->id_bill)->first();

?>
                        <tr>
                            <td><label class="i-checks m-b-none"><input type="checkbox" name="post[]"><i></i></label></td>
                            <td>{{$product->product_name}}</td>
                            <td><b style="color:#FE980F;">{{$product->code_product}}</b></td>
                            @if($payment->payment_method==1||$payment->payment_method==2)
                            <td>@if($product->product_price_km!=0){{number_format($product->product_price_km,0,',','.')}}@else {{number_format($product->product_price,0,',','.')}} @endif  VNĐ</td>
                            <td><b style="color:#FE980F;">{{$d->quantity}}</b></td>
                            <td><b style="color:#FE980F;">{{$bill->total}} VNĐ</b></td>
                            <td><b style="color:#FE980F;">@if($payment->payment_method==1)Chuyển khoản @elseif($payment->payment_method==2) Thanh toán khi nhận hàng @endif</b></td>
                            @elseif($payment->payment_method==3)

                                <?php
                                $price_km=($product->product_price_km*800)/20000;
                                $price=($product->product_price*800)/20000;
                                ?>
                                <td>@if($product->product_price_km!=0){{number_format($price_km,0,',','.')}}@else {{number_format($price,0,',','.')}} @endif  BitCoinSJC</td>
                                <td><b style="color:#FE980F;">{{$d->quantity}}</b></td>
                                <td><b style="color:#FE980F;">{{number_format($bill->total,0,',','.')}} BitCoinSJC</b></td>

                                    <td><b style="color:#FE980F;">BitCoinSJC</b></td>
                            @endif
                            <td>
                                @if($bill->status!=3&&$bill->status!=2)
                                <a href="{{asset('admin/order/edit/'.$d->id_bill_detail)}}" class="active" ui-toggle-class=""><i class="fa fa-pencil text-success text-active"></i></a>&nbsp;&nbsp;&nbsp;


                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
            <a href="{{asset('admin/shop/print/'.$bill->code_bill)}}"style="color: #fe980F" target="_blank">In đơn hàng</a>
            <footer class="panel-footer">
                <div class="row">

                    <div class="col-sm-7 text-right text-center-xs">
                        {{$billdetail->links('admin.paginate')}}
                    </div>
                </div>
            </footer>
        </div>
    </div>

@endsection

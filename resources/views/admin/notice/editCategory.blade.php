@extends('admin.master');
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Sửa
                </header>
                <div class="panel-body">
                    <div class="position-center">

                        <form role="form" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                @include('admin.errors.error')
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tiêu đề </label>
                                <input type="text" class="form-control" value="{{$notice->title}}" name="title" id="exampleInputEmail1" placeholder="Tên ">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Nội dung </label>
                                <textarea name="content" class="form-control" id="product_content" >{{$notice->content}}</textarea>


                                <script>
                                    CKEDITOR.replace( 'product_content',
                                        {
                                            filebrowserBrowseUrl : '{{asset("public/admin/ckfinder/ckfinder.html")}}',
                                            filebrowserImageBrowseUrl : '{{asset("public/admin/ckfinder/ckfinder.html?type=Images")}}',
                                            filebrowserFlashBrowseUrl : '{{asset("public/admin/ckfinder/ckfinder.html?type=Flash")}}',
                                            filebrowserUploadUrl : '{{asset("public/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files")}}',
                                            filebrowserImageUploadUrl : '{{asset("public/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images")}}',
                                            filebrowserFlashUploadUrl : '{{asset("public/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash")}}'
                                        });
                                </script>

                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Loại</label>
                                <select class="form-control input-sm m-bot-15" name="type">
                                    <option value="0">chon</option>

                                    <option value="0" @if($notice->type==0) selected @endif>Thông báo</option>
                                    <option value="1"  @if($notice->type==1) selected @endif>Thư</option>
{{--                                    @foreach($shop as $s)--}}
{{--                                        <option value="{{$s->id_shop_user}}-{{$s->name_shop}}" @if($notice->type==0) selected @endif>{{$s->name_shop}}</option>--}}
{{--                                    @endforeach--}}

                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Shop</label>
                                <select class="form-control input-sm m-bot-15" name="id_shop">
                                    <option value="0">chon</option>

                                    @foreach($shop as $s)
                                        <option value="{{$s->id_shop_user}}" @if($notice->id_shop==$s->id_shop_user) selected @endif>{{$s->username}}</option>
                                    @endforeach

                                </select>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Customer</label>
                                <select class="form-control input-sm m-bot-15" name="id_shop">
                                    <option value="0">chon</option>

                                    @foreach($shop as $s)
                                        <option value="{{$s->id_customer}}" @if($notice->id_customer==$s->id_customer) selected @endif>{{$s->customer_email}}</option>
                                    @endforeach

                                </select>
                            </div>
                            <button type="submit" class="btn btn-info">Submit</button>
                        </form>
                    </div>

                </div>
            </section>

        </div>

    </div>
@endsection
<?php

function showCategories($categories,$parent_id=0,$char=''){
    foreach($categories as $key =>$item){
        if($item->parent_id==$parent_id){
            echo '<option value="'.$item->id_category_product.'">'.$char.$item->category_name.'</option>';
            unset($categories[$key]);
            showCategories($categories,$item->id_category_product,$char=' | --');
        }
    }
}
?>

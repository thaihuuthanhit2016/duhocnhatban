<!-- Important Owl stylesheet -->
<link rel="stylesheet" href="{{asset('public/slider/owl-carousel/owl.carousel.css')}}">

<!-- Default Theme -->
<link rel="stylesheet" href="{{asset('public/slider/owl-carousel/owl.theme.css')}}">

<footer id="footer"><!--Footer-->
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-2">
                    <div class="companyinfo">
                        <h2><span>e</span>-shopper</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,sed do eiusmod tempor</p>
                    </div>
                </div>
                <div class="col-sm-7">
                    <div id="owl-example" class="owl-carousel">
                        <div>   <img width="100%"  src="{{asset('public/Eshopper/images/home/iframe4.png')}}" alt="" /><br>
                        </div>
                        <div>   <img width="100%"  src="{{asset('public/Eshopper/images/home/iframe4.png')}}" alt="" /><br>
                        </div>
                        <div>    <img width="100%"  src="{{asset('public/Eshopper/images/home/iframe4.png')}}" alt="" /><br>
                            </div>
                        <div>    <img width="100%"  src="{{asset('public/Eshopper/images/home/iframe4.png')}}" alt="" /><br>
                            </div>
                        <div>    <img width="100%"  src="{{asset('public/Eshopper/images/home/iframe4.png')}}" alt="" /><br>
                            </div>
                        <div>    <img width="100%"  src="{{asset('public/Eshopper/images/home/iframe4.png')}}" alt="" /><br>
                            </div>
                        <div>    <img width="100%"  src="{{asset('public/Eshopper/images/home/iframe4.png')}}" alt="" /><br>
                            </div>

                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="address">
                        <img src="{{asset('public/Eshopper/images/home/map.png')}}" alt="" />
                        <p>505 S Atlantic Ave Virginia Beach, VA(Virginia)</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-widget">
        <div class="container">
            <div class="row">
                <div class="col-sm-2">
                    <div class="single-widget">
                        <h2>Chính sách</h2>
                        <ul class="nav nav-pills nav-stacked">
                            <?php
                            $chinhsach=DB::table('tbl_pages')->where('type',0)->get();
                            $huongdan=DB::table('tbl_pages')->where('type',1)->get();
                            $khuyenmai=DB::table('tbl_pages')->where('type',2)->get();
                            $dichvu=DB::table('tbl_pages')->where('type',3)->get();
                            ?>
                            @foreach($chinhsach as $c)
                                <li><a href="{{asset('pages/'.$c->title_slug)}}">{{$c->title}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="single-widget">
                        <h2>Hướng dẫn</h2>
                        <ul class="nav nav-pills nav-stacked">
                            @foreach($huongdan as $c)
                                <li><a href="{{asset('pages/'.$c->title_slug)}}">{{$c->title}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="single-widget">
                        <h2>Khuyến mãi</h2>
                        <ul class="nav nav-pills nav-stacked">

                            @foreach($khuyenmai as $c)
                                <li><a href="{{asset('pages/'.$c->title_slug)}}">{{$c->title}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="single-widget">
                        <h2>Dịch vụ</h2>
                        <ul class="nav nav-pills nav-stacked">

                            @foreach($dichvu as $c)
                                <li><a href="{{asset('pages/'.$c->title_slug)}}">{{$c->title}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3 col-sm-offset-1">
                    <div class="single-widget">
                        <h2>About Shopper</h2>
                        <form action="#" class="searchform">
                            <input type="text" placeholder="Your email address" />
                            <button type="submit" class="btn btn-default"><i class="fa fa-arrow-circle-o-right"></i></button>
                            <p>Get the most recent updates from <br />our site and be updated your self...</p>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <p class="pull-left">Copyright © 2018 BitCoinSJC Inc. All rights reserved.</p>
                <p class="pull-right">Designed by BitCoinSJC and Sponsored by BitCoinSJC</p>
            </div>
        </div>
    </div>

</footer><!--/Footer-->



<script src="{{asset('public/Eshopper/js/jquery.js')}}"></script>
<script src="{{asset('public/Eshopper/js/bootstrap.min.js')}}"></script>
<script src="{{asset('public/Eshopper/js/jquery.scrollUp.min.js')}}"></script>
<script src="{{asset('public/Eshopper/js/price-range.js')}}"></script>
<script src="{{asset('public/Eshopper/js/jquery.prettyPhoto.js')}}"></script>
<script src="{{asset('public/Eshopper/js/main.js')}}"></script>

</body>
</html>

<script>
    $('.qty_1').keyup(function (){
        var id=$('.id_product_1').val();
        var qty=$('.qty_1').val();

        $.ajax({
            url : "{{asset('updatecart')}}",
            type : "get",
            dataType:"text",
            data : {
                id : id,
                qty : qty
            },
            success : function (result){
                var str=result;
                var tach=str.split('-');
                $('.result_1').html(tach[0]);
                $('#subtotal').html(tach[1]);
            }
        });


    });
    $('.qty_2').keyup(function (){
        var id=$('.id_product_2').val();
        var qty=$('.qty_2').val();

        $.ajax({
            url : "{{asset('updatecart')}}",
            type : "get",
            dataType:"text",
            data : {
                id : id,
                qty : qty
            },
            success : function (result){
                var str=result;
                var tach=str.split('-');
                $('.result_2').html(tach[0]);
                $('#subtotal').html(tach[1]);
            }
        });


    });
</script>
<style>
    .owl-nav {
        text-align: center;
    }.owl-nav span {
         font-size: 44px;

     }
</style>

<style>
    #slider_modal,#slider_modal1,#slider_modal2{
        width: 200px;
        height: 400px;
        overflow: hidden;
    }
    #slider_modal .sliders_modal,#slider_modal1 .sliders_modal1,#slider_modal2 .sliders_modal2{
        display: block;
        width: 6000px;
        height: 400px;
        margin: 0;
        padding: 0;

    }
    #slider_modal .slide_modal,#slider_modal1 .slide_modal1,#slider_modal2 .slide_modal2{
        float: left;
        list-style-type: none;
        width: 200px;
        height: 400px;
    }
</style>

            <script src="http://localhost/shopbitcoin/public/Eshopper/js/lightslider.js"></script>
            <script src="http://localhost/shopbitcoin/public/Eshopper/js/lightgallery-all.min.js"></script>
            <script>

                $('#imageGallery2').addClass('none');
                $('#imageGallery2 .lslide').addClass('none');
                $('#imageGallery2').lightSlider({
                    gallery:true,
                    item:1,
                    loop:false,
                    thumbItem:9,
                    slideMargin:0,
                    enableDrag: true,
                    width:100,
                    height:100,
                    currentPagerPosition:'left',
                    onSliderLoad: function(el) {
                        el.lightGallery({
                            selector: '#imageGallery .lslide'
                        });
                    }
                });
            </script>
        </div>

    </div>

<style>
    .oooo{
        width: 100px!important;
    }
    .form-control2{

        width: 20%!important;

    }
    .none{

        width: 100%!important;
        height: auto!important;
    }
</style>

<script src="{{asset('public/slider/owl-carousel/owl.carousel.js')}}"></script>
<script>
    $(document).ready(function() {
        var owl = $('#owl-example');
        owl.owlCarousel({
            items:4,
            loop:true,
            margin:10,
            autoPlay:true,
            autoplayTimeout:1000,
            autoplayHoverPause:true
        });
        owl.trigger('play.owl.autoplay',[1000])

    });
</script>
<script>
    $(function (){
        var width=200;
        var animationSpeed=1000;
        var pause=3000;
        var curentSlide=1;


        setInterval(function (){

            $('#slider_modal .sliders_modal').animate({'margin-left':'-='+width+'px'},animationSpeed,function (){
                curentSlide++;

                if(curentSlide===$('.slide_modal').length){
                    curentSlide=1;
                    $('#slider_modal .sliders_modal').css('margin-left',0);
                }
            });
        },pause);
        setInterval(function (){

            $('#slider_modal1 .sliders_modal1').animate({'margin-left':'-='+width+'px'},animationSpeed,function (){
                curentSlide++;

                if(curentSlide===$('.slide_modal1').length){
                    curentSlide=1;
                    $('#slider_modal1 .sliders_modal1').css('margin-left',0);
                }
            });
        },pause);

        setInterval(function (){

            $('#slider_modal2 .sliders_modal2').animate({'margin-left':'-='+width+'px'},animationSpeed,function (){
                curentSlide++;
console.log($('.slide_modal2').length);
                if(curentSlide===$('.slide_modal2').length){
                    curentSlide=1;
                    $('#slider_modal2 .sliders_modal2').css('margin-left',0);
                }
            });
        },pause);
    });
</script>

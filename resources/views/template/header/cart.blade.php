<header id="header"><!--header-->
    <div class="header_top"><!--header_top-->
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="contactinfo">
                        <ul class="nav nav-pills">
                            <li><a href="#"><i class="fa fa-phone"></i> +2 95 01 88 821</a></li>
                            <li><a href="#"><i class="fa fa-envelope"></i> info@domain.com</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="social-icons pull-right">
                        <ul class="nav navbar-nav">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header_top-->

    <div class="header-middle"><!--header-middle-->
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="logo pull-left">
                        <a href="index.html"><img src="{{asset('public/Eshopper/images/home/logo.png')}}" alt="" /></a>
                    </div>

                </div>
                <div class="col-sm-8">
                    <div class="shop-menu pull-right">
                        <ul class="nav navbar-nav">@if(Session::get('customer_id')!='')
                                <li><a href="{{asset('account')}}"><i class="fa fa-user"></i> Tài khoản</a></li>

                                <li><a href="#"><i class="fa fa-star"></i> Yêu thích</a></li>
                            @endif
                            <li><a href="{{asset('checkout')}}"><i class="fa fa-crosshairs"></i> Thanh toán</a></li>
                            <li><a href="{{asset('cart')}}"><i class="fa fa-shopping-cart"></i> Giỏ hàng @if(count(Cart::content())>0)<sup style="color:red;">({{count(Cart::content())}})</sup>@endif</a></li>
                            @if(!Session::get('customer_id'))
                                <li><a href="{{asset('login')}}"><i class="fa fa-lock"></i> Đăng nhập</a></li>
                            @else
                                <li><a href="{{asset('logoutacount')}}"><i class="fa fa-lock"></i> Đăng xuất</a></li>
                            @endif                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header-middle-->

    <div class="header-bottom"><!--header-bottom-->
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="mainmenu pull-left">
                        <ul class="nav navbar-nav collapse navbar-collapse">
                            <li><a href="{{asset('/')}}" class="active">Trang chủ</a></li>
                            <li class="dropdown"><a href="{{asset('product')}}">Sản phẩm</a>

                            </li>
                            <li class="dropdown"><a href="{{asset('news')}}">Tin tức</a>

                            </li>
                            <li><a href="{{asset('contact')}}">Liên hệ</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="search_box pull-right">
                        <form method="post" action="{{asset('search')}}">
                            @csrf
                            <input  type="text" name="keyword" placeholder="Tìm kiếm sản phẩm"/>
                            <input  style="margin-top: 0px;color:white;"type="submit" class="btn btn-primary" value="Tìm"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header-bottom-->
</header><!--/header-->

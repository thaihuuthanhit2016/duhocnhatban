<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\CategoryProductController;
use App\Http\Controllers\BrandProductController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\OfferController;
use App\Http\Controllers\SliderController;
use App\Http\Controllers\MailController;
use App\Http\Controllers\ShopController;
use App\Http\Controllers\DeliveryController;
use App\Http\Controllers\CategoryNewsController;
use App\Http\Controllers\NoticeController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\Multiple_choiceController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\CarController;
use App\Http\Controllers\FeedbackController;
use App\Http\Controllers\ShipperController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/car', 'App\Http\Controllers\HomeController@getCar');
Route::get('/detailcar/{id}', 'App\Http\Controllers\HomeController@detailcar');

Route::get('/', [HomeController::class, 'index']);
Route::get('/loadcomment', [HomeController::class, 'loadcomment']);
Route::get('/autocomplect_ajax_search', [HomeController::class, 'autocomplet_ajax_search']);
Route::get('/loadquestionresult', [HomeController::class, 'loadquestionresult']);
Route::get('/view-result', [HomeController::class, 'view_result']);
Route::get('/compare/{id}', [HomeController::class, 'compare']);
Route::get('/compare', [HomeController::class, 'compare_customer']);
Route::get('/spin', [HomeController::class, 'spin']);
Route::get('/multiple_choice', [HomeController::class, 'multiple_choice']);
Route::get('/loadquestion', [HomeController::class, 'loadquestion']);
Route::post('/multiple_choice', [HomeController::class, 'checkresult']);
Route::get('/multiple_choices', [HomeController::class, 'checkresult']);
Route::get('/pages/{id}', [HomeController::class, 'getPages']);
Route::get('/tags/{id}', [HomeController::class, 'detail_tags']);
Route::get('/qr', [HomeController::class, 'test']);
Route::get('/test', [HomeController::class, 'testdata']);
Route::get('/addcomment', [HomeController::class, 'addcomment']);
Route::get('/repleycomment', [HomeController::class, 'repleycomment']);
Route::get('/news', [HomeController::class, 'news']);
Route::get('/detail-news/{id}', [HomeController::class, 'detailnews']);
Route::get('/category_news/{id}', [HomeController::class, 'category_news']);
Route::get('/getdata', [HomeController::class, 'GetList']);
Route::get('/404', [HomeController::class, 'get404']);
Route::get('/loto', [HomeController::class, 'getLoto']);
Route::get('/trang-chu', [HomeController::class, 'index']);
Route::post('/search', [HomeController::class, 'search']);
Route::get('/category/{id}', [HomeController::class, 'category']);
Route::get('/change-password', [HomeController::class, 'changepass']);
Route::get('/order', [HomeController::class, 'order']);
Route::get('/wishlist/{id}', [HomeController::class, 'wishlist']);
Route::get('/wishlist', [HomeController::class, 'wishlist_user']);
Route::get('/rating', [HomeController::class, 'rating']);
Route::get('/wallet', [HomeController::class, 'wallet_user']);
Route::post('/doOffer', [HomeController::class, 'doOffer']);
Route::post('/change-password', [HomeController::class, 'postchangepass']);
Route::get('/brands/{id}', [HomeController::class, 'brands']);
Route::get('/detail/{id}', [HomeController::class, 'detail']);
Route::get('/add-to-cart/{id}', [HomeController::class, 'AddToCart']);
Route::post('/add-to-cart', [HomeController::class, 'PostAddToCart']);
Route::get('/delCart/{id}', [HomeController::class, 'DelCart']);
Route::get('/cart', [HomeController::class, 'ShowCart']);
Route::get('/buycard/{id}', [HomeController::class, 'BuyCard']);
Route::get('/buycard', [HomeController::class, 'BuyCard1']);
Route::get('/payment/buycard/{id}', [HomeController::class, 'PaymentBuyCard']);
Route::get('/getBill/{id}', [HomeController::class, 'GetBill']);
Route::post('/updatecart', [HomeController::class, 'UpdateCart']);
Route::get('/login/checkout', [HomeController::class, 'LoginCheckout']);
Route::get('/login', [HomeController::class, 'LoginCheckout']);
Route::get('/introduce/', [HomeController::class, 'getIntroduce']);
Route::get('/paymented/{id}', [HomeController::class, 'Paymented']);
Route::post('/update_profile', [HomeController::class, 'update_profile']);
Route::post('/register', [HomeController::class, 'Register']);
Route::get('/contact', [HomeController::class, 'getContact']);
Route::get('/game', [HomeController::class, 'game']);
Route::post('/contact', [HomeController::class, 'postContact']);
Route::post('/login', [HomeController::class, 'Login']);
Route::get('/checkout', [HomeController::class, 'Checkout']);
Route::get('/chatting', [HomeController::class, 'Chat']);
Route::post('/checkout', [HomeController::class, 'postCheckout']);
Route::get('/send-mail', [HomeController::class, 'sendmail']);
Route::get('/account', [HomeController::class, 'account']);
Route::get('/payment', [HomeController::class, 'payment']);
Route::get('/order-success', [HomeController::class, 'orderSuccess']);
Route::get('/order-successed', [HomeController::class, 'orderSuccessed']);
Route::post('/payment', [HomeController::class, 'postpayment']);
Route::get('/viewed_producut', [HomeController::class, 'viewed_producut']);
Route::get('/logoutacount', [HomeController::class, 'getLogoutAccount']);
Route::get('/offer', [HomeController::class, 'offer']);
Route::get('/shop/{id}', [HomeController::class, 'shop']);
Route::get('/loginfacebook', [HomeController::class, 'loginfacebook']);
Route::get('/callback', [HomeController::class, 'callback']);
Route::get('/product', [HomeController::class, 'product']);
Route::post('/update_wallet', [HomeController::class, 'postUpdate_wallet']);
Route::get('/quickview', [HomeController::class, 'quickview']);
Route::post('/promotion_information', [HomeController::class, 'promotion_information']);
Route::get('/del/cart/{id}', [HomeController::class, 'delCarrt']);
Route::get('/autocomplect_tinh', [HomeController::class, 'autocomplect_tinh']);
Route::get('/api/show', [HomeController::class, 'api_show']);
//mail



Route::get('/admin/email', [MailController::class, 'getMail']);
Route::get('/admin/writenew', [MailController::class, 'getWritenew']);
Route::post('/admin/writenew', [MailController::class, 'postWritenew']);
Route::get('/admin/send', [MailController::class, 'send']);
Route::get('/admin/viewed/{id}', [MailController::class, 'view']);
Route::get('/admin/reply/{id}', [MailController::class, 'reply']);
Route::post('/admin/reply/{id}', [MailController::class, 'postreply']);



Route::get('/admin', [AdminController::class, 'index']);
Route::get('/logout', [AdminController::class, 'getLogout']);
Route::get('/admin/dashboard', [AdminController::class, 'showDashboard']);
Route::post('/admin', [AdminController::class, 'postDashboard']);


Route::group(['prefix' => '/admin'], function () {
    Route::get('/loadmessage', [AdminController::class, 'loadmessage']);
    Route::get('/setting/{id}', [AdminController::class, 'getsetting']);
    Route::post('/setting/{id}', [AdminController::class, 'postsetting']);
    Route::get('/adminloadmessage', [AdminController::class, 'adminloadmessage']);
    Route::get('/support', [AdminController::class, 'getsupport']);
    Route::get('/loaduser', [AdminController::class, 'getUser']);
    Route::get('/chat/{id}', [AdminController::class, 'getChat']);
    Route::post('/support', [AdminController::class, 'postsupport']);
    Route::get('/messeage/{id}', [AdminController::class, 'getMesseage']);
    Route::get('/filter-by-date', [AdminController::class, 'getFilter_by_date']);
    Route::get('/dashboard-filter', [AdminController::class, 'getFilter']);
    Route::get('/30day', [AdminController::class, 'get30day']);

    Route::post('/export-csv',[ProductController::class, 'export_csv']);
    Route::post('/approveComment',[ProductController::class, 'approveComment']);
    Route::post('/import-csv',[ProductController::class, 'import_csv']);


    Route::group(['prefix' => '/comment'], function () {
        Route::get('/approveComment/{id}',[AdminController::class, 'approveComment']);
        Route::get('/list',[AdminController::class, 'listComment']);
        Route::get('/rejectComment/{id}',[AdminController::class, 'rejectComment']);

    });
    Route::group(['prefix' => '/withraw'], function () {
        Route::get('/add',[ShopController::class, 'getAddWithraw']);
        Route::get('/edit/{id}',[ShopController::class, 'getEditWithraw']);
        Route::post('/edit/{id}',[ShopController::class, 'postEditWithraw']);
        Route::post('/add',[ShopController::class, 'posttAddWithraw']);
        Route::get('/list',[ShopController::class, 'listWithraw']);

    });
    Route::group(['prefix' => '/feedback'], function () {
        Route::get('/add',[ShopController::class, 'getAddWithraw']);
        Route::get('/edit/{id}',[ShopController::class, 'getEditWithraw']);
        Route::post('/edit/{id}',[ShopController::class, 'postEditWithraw']);
        Route::post('/add',[ShopController::class, 'posttAddWithraw']);
        Route::get('/list',[FeedbackController::class, 'getListCategory']);

    });
    Route::group(['prefix' => '/pages'], function () {
        Route::get('/del/{id}', [PagesController::class, 'getDelCategory']);
        Route::get('/edit/{id}', [PagesController::class, 'getEditCategory']);
        Route::post('/edit/{id}', [PagesController::class, 'postEditCategory']);
        Route::get('/unactive/{id}', [PagesController::class, 'getUnactive']);
        Route::get('/active/{id}', [PagesController::class, 'getActive']);
        Route::get('/add', [PagesController::class, 'getAddCategory']);
        Route::get('/list', [PagesController::class, 'getListCategory']);
        Route::post('/add', [PagesController::class, 'postAddCategory']);
    });

    Route::group(['prefix' => '/multiple-choice'], function () {
        Route::get('/del/{id}', [Multiple_choiceController::class, 'getDelCategory']);
        Route::get('/edit/{id}', [Multiple_choiceController::class, 'getEditCategory']);
        Route::post('/edit/{id}', [Multiple_choiceController::class, 'postEditCategory']);
        Route::get('/unactive/{id}', [Multiple_choiceController::class, 'getUnactive']);
        Route::get('/active/{id}', [Multiple_choiceController::class, 'getActive']);
        Route::get('/add', [Multiple_choiceController::class, 'getAddCategory']);
        Route::get('/list', [Multiple_choiceController::class, 'getListCategory']);
        Route::post('/add', [Multiple_choiceController::class, 'postAddCategory']);
    });
    Route::group(['prefix' => '/card'], function () {
        Route::get('/list', [AdminController::class, 'getCard']);
        Route::get('/edit/{id}', [AdminController::class, 'editCard']);
        Route::post('/edit/{id}', [AdminController::class, 'posteditCard']);

    });

    Route::group(['prefix' => '/category'], function () {
        Route::get('/del/{id}', [CategoryProductController::class, 'getDelCategory']);
        Route::get('/edit/{id}', [CategoryProductController::class, 'getEditCategory']);
        Route::post('/edit/{id}', [CategoryProductController::class, 'postEditCategory']);
        Route::get('/unactive/{id}', [CategoryProductController::class, 'getUnactive']);
        Route::get('/active/{id}', [CategoryProductController::class, 'getActive']);
        Route::get('/add', [CategoryProductController::class, 'getAddCategory']);
        Route::get('/list', [CategoryProductController::class, 'getListCategory']);
        Route::post('/add', [CategoryProductController::class, 'postAddCategory']);
    });

    Route::group(['prefix' => '/category-news'], function () {
        Route::get('/del/{id}', [CategoryNewsController::class, 'getDelCategory']);
        Route::get('/edit/{id}', [CategoryNewsController::class, 'getEditCategory']);
        Route::post('/edit/{id}', [CategoryNewsController::class, 'postEditCategory']);
        Route::get('/unactive/{id}', [CategoryNewsController::class, 'getUnactive']);
        Route::get('/active/{id}', [CategoryNewsController::class, 'getActive']);
        Route::get('/add', [CategoryNewsController::class, 'getAddCategory']);
        Route::get('/list', [CategoryNewsController::class, 'getListCategory']);
        Route::post('/add', [CategoryNewsController::class, 'postAddCategory']);
    });

    Route::group(['prefix' => '/brands'], function () {
        Route::get('/del/{id}', [BrandProductController::class, 'getDelCategory']);
        Route::get('/edit/{id}', [BrandProductController::class, 'getEditCategory']);
        Route::post('/edit/{id}', [BrandProductController::class, 'postEditCategory']);
        Route::get('/add', [BrandProductController::class, 'getAddCategory']);
        Route::get('/list', [BrandProductController::class, 'getListCategory']);
        Route::post('/add', [BrandProductController::class, 'postAddCategory']);

        Route::get('/unactive/{id}', [BrandProductController::class, 'getUnactive']);
        Route::get('/active/{id}', [BrandProductController::class, 'getActive']);
    });


    Route::group(['prefix' => '/product'], function () {
        Route::get('/del/{id}', [ProductController::class, 'getDelCategory']);
        Route::get('/product_images/del/{id}', [ProductController::class, 'getDelImages']);
        Route::get('/edit/{id}', [ProductController::class, 'getEditCategory']);
        Route::post('/edit/{id}', [ProductController::class, 'postEditCategory']);
        Route::get('/view/{id}', [ProductController::class, 'getViewCategory']);
        Route::get('/add', [ProductController::class, 'getAddCategory']);
        Route::get('/serachproduct', [ProductController::class, 'serachproduct']);
        Route::get('/list', [ProductController::class, 'getListCategory']);
        Route::get('/list_', [ProductController::class, 'getListCategory_']);
        Route::post('/add', [ProductController::class, 'postAddCategory']);

        Route::get('/unactive/{id}', [ProductController::class, 'getUnactive']);
        Route::get('/active/{id}', [ProductController::class, 'getActive']);
    });
    Route::group(['prefix' => '/car'], function () {
        Route::get('/del/{id}', [CarController::class, 'getDelCategory']);
        Route::get('/product_images/del/{id}', [CarController::class, 'getDelImages']);
        Route::get('/edit/{id}', [CarController::class, 'getEditCategory']);
        Route::post('/edit/{id}', [CarController::class, 'postEditCategory']);
        Route::get('/view/{id}', [CarController::class, 'getViewCategory']);
        Route::get('/add', [CarController::class, 'getAddCategory']);
        Route::get('/serachproduct', [CarController::class, 'serachproduct']);
        Route::get('/list', [CarController::class, 'getListCategory']);
        Route::get('/list_', [CarController::class, 'getListCategory_']);
        Route::post('/add', [CarController::class, 'postAddCategory']);

        Route::get('/unactive/{id}', [CarController::class, 'getUnactive']);
        Route::get('/active/{id}', [CarController::class, 'getActive']);
    });
    Route::group(['prefix' => '/delivery'], function () {
        Route::get('/del/{id}', [DeliveryController::class, 'getDelCategory']);
        Route::get('/province', [DeliveryController::class, 'getProvince']);
        Route::get('/product_images/del/{id}', [DeliveryController::class, 'getDelImages']);
        Route::get('/list', [DeliveryController::class, 'getEditCategory']);
        Route::get('/add', [DeliveryController::class, 'getAddCategory']);
        Route::get('/fee', [DeliveryController::class, 'getFee']);
        Route::get('/edit', [DeliveryController::class, 'postEditCategory']);
        Route::get('/listall', [DeliveryController::class, 'getListCategory']);


        Route::get('/unactive/{id}', [DeliveryController::class, 'getUnactive']);
        Route::get('/active/{id}', [DeliveryController::class, 'getActive']);
    });
    Route::group(['prefix' => '/slider'], function () {
        Route::get('/del/{id}', [SliderController::class, 'getDelCategory']);
        Route::get('/product_images/del/{id}', [SliderController::class, 'getDelImages']);
        Route::get('/edit/{id}', [SliderController::class, 'getEditCategory']);
        Route::post('/edit/{id}', [SliderController::class, 'postEditCategory']);
        Route::get('/view/{id}', [SliderController::class, 'getViewCategory']);
        Route::get('/add', [SliderController::class, 'getAddCategory']);
        Route::get('/list', [SliderController::class, 'getListCategory']);
        Route::post('/add', [SliderController::class, 'postAddCategory']);

        Route::get('/unactive/{id}', [SliderController::class, 'getUnactive']);
        Route::get('/active/{id}', [SliderController::class, 'getActive']);
    });
    Route::group(['prefix' => '/offer'], function () {
        Route::get('/del/{id}', [OfferController::class, 'getDelCategory']);
        Route::get('/edit/{id}', [OfferController::class, 'getEditCategory']);
        Route::post('/edit/{id}', [OfferController::class, 'postEditCategory']);
        Route::get('/view/{id}', [OfferController::class, 'getViewCategory']);
        Route::get('/add', [OfferController::class, 'getAddCategory']);
        Route::get('/list', [OfferController::class, 'getListCategory']);
        Route::post('/add', [OfferController::class, 'postAddCategory']);

        Route::get('/unactive/{id}', [OfferController::class, 'getUnactive']);
        Route::get('/active/{id}', [OfferController::class, 'getActive']);
    });
    Route::group(['prefix' => '/news'], function () {
        Route::get('/del/{id}', [NewsController::class, 'getDelCategory']);
        Route::get('/edit/{id}', [NewsController::class, 'getEditCategory']);
        Route::post('/edit/{id}', [NewsController::class, 'postEditCategory']);
        Route::get('/view/{id}', [NewsController::class, 'getViewCategory']);
        Route::get('/add', [NewsController::class, 'getAddCategory']);
        Route::get('/list', [NewsController::class, 'getListCategory']);
        Route::post('/add', [NewsController::class, 'postAddCategory']);

        Route::get('/unactive/{id}', [OfferController::class, 'getUnactive']);
        Route::get('/active/{id}', [OfferController::class, 'getActive']);
    });

    Route::group(['prefix' => '/order'], function () {
        Route::get('/list', [ShopController::class, 'getAllOrdershop']);
        Route::get('/viewordered/{id}', [ShopController::class, 'getDeltailOrder']);
        Route::get('/edit/{id}', [ShopController::class, 'getEditorder']);
        Route::post('/edit/{id}', [ShopController::class, 'postEditorder']);
    });
    Route::group(['prefix' => '/shop'], function () {
        Route::get('/list', [ShopController::class, 'getListCategory']);
        Route::get('/print/{id}', [ShopController::class, 'print']);
        Route::get('/view/{id}', [ShopController::class, 'getViewCategory']);
        Route::get('/approve/{id}', [ShopController::class, 'getApproveCategory']);
        Route::get('/reject/{id}', [ShopController::class, 'getRejectCategory']);
        Route::get('/loginofshop/{id}', [ShopController::class, 'loginofshop']);

        Route::get('/info', [AdminController::class, 'infoShop']);
        Route::get('/apicoin', [AdminController::class, 'getApicoin']);
        Route::post('/info', [AdminController::class, 'postAddCategory']);

    });
    Route::group(['prefix' => '/shipper'], function () {
        Route::get('/list', [ShipperController::class, 'getListCategory']);
        Route::get('/print/{id}', [ShipperController::class, 'print']);
        Route::get('/view/{id}', [ShipperController::class, 'getViewCategory']);
        Route::get('/approve/{id}', [ShipperController::class, 'getApproveCategory']);
        Route::get('/reject/{id}', [ShipperController::class, 'getRejectCategory']);
        Route::get('/loginofshop/{id}', [ShipperController::class, 'loginofshop']);

        Route::get('/info', [AdminController::class, 'infoShipper']);
        Route::get('/apicoin', [AdminController::class, 'getApicoin']);
        Route::post('/info', [AdminController::class, 'postShipper']);

    });
    Route::group(['prefix' => '/notice'], function () {
        Route::get('/del/{id}', [NoticeController::class, 'getDelCategory']);
        Route::get('/product_images/del/{id}', [NoticeController::class, 'getDelImages']);
        Route::get('/edit/{id}', [NoticeController::class, 'getEditCategory']);
        Route::post('/edit/{id}', [NoticeController::class, 'postEditCategory']);
        Route::get('/view/{id}', [NoticeController::class, 'getViewCategory']);
        Route::get('/add', [NoticeController::class, 'getAddCategory']);
        Route::get('/list', [NoticeController::class, 'getListCategory']);
        Route::post('/add', [NoticeController::class, 'postAddCategory']);

        Route::get('/unactive/{id}', [NoticeController::class, 'getUnactive']);
        Route::get('/active/{id}', [NoticeController::class, 'getActive']);

    });
});
